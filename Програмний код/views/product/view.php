<?php
/** @var array $product */
/** @var array $statistics_id */
/** @var array $format_prog */
?>
    <style>
        body {
            background-color: #f7f7f7;
        }

        .blok-format {
            border-radius: 5px;
            padding: 3px;
            padding-right: 9px;
            padding-left: 9px;
            width: auto;
        }

        .name-size {
            font-size: 14px;
        }

        .topradius {
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
        }

        .bottomradius {
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .outer {
            height: 100%;
            vertical-align: middle;
            position: relative;
        }

        .TypePolygon {
            background: url("/static/images/poligon-vertex.jpg") no-repeat;
            background-size: 100%;
            width: 30px;
            height: 30px;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
        }

        .TypeTexture {
            background: url("/static/images/texture.jpg") no-repeat;
            background-size: 100%;
            width: 30px;
            height: 30px;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
        }
        .TypeText {
            background-size: 100%;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
        }

        .nativeformat {
            margin-left: 3px;
            margin-right: 3px;
            border-radius: 0;
        }

    </style>


    <div class="container">
        <div class="row">
            <div style="width: 1280px" class="col-12 mb-3">
                <?php
                if (is_string($product['photos']))
                    $filePath = 'files/product/' . $product['name'] . '/' . $product['photos'];
                if (is_array($product['photos']))
                    $filePath = 'files/product/' . $product['name'] . '/' . $product['photos'][0];
                ?>
                <?php if (is_file($filePath)) : ?>
                <div id="carouselExampleIndicators" class="carousel slide">

                    <div class="carousel-inner">
                        <?php if (is_array($product['photos'])) : ?>
                        <div class="carousel-item active">
                            <img src="/<?= 'files/product/' . $product['name'] . '/' . $product['photos'][0]; ?>"
                                 class="d-block w-100"
                                 alt="...">
                        </div>
                        <? for ($i = 1; $i < count($product['photos']); $i++): ?>
                            <div class="carousel-item">
                                <img src="/<?= 'files/product/' . $product['name'] . '/' . $product['photos'][$i]; ?>"
                                     class="d-block w-100"
                                     alt="...">
                            </div>
                        <? endfor; ?>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                            data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                            data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                    <? elseif (is_string($product['photos'])): ?>
                        <div class="carousel-item active">
                            <img src="/<?= 'files/product/' . $product['name'] . '/' . $product['photos'] ?>"
                                 class="d-block w-100" alt="...">
                        </div>
                    <? endif; ?>
                </div>
            </div>
            <?php else: ?>
                <img src="/static/images/no-image.jpg" class="img-thumbnail" alt="">
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="alert alert-secondary" role="alert">
                <div class="row mt-2">
                    <div class="col-6">
                        <h1 class="h3 mb-3 fw-normal text-start"><?= $product['name'] ?></h1>
                    </div>
                    <div class="col-6">
                        <form  action="" method="post" class="text-end">
                            <strong class="link-success"><?= $product['price']; ?>$ </strong>
                            <input min="0" type="hidden" id="product_id" name="product_id" value="<?= $product['id'] ?>">
                            <button class="btn btn-warning" type="submit">Купити</button>
                        </form>
                        <?php if (!empty($errors['name'])): ?>
                            <div class="form-text text-danger"> <?= $errors['name']; ?></div>
                        <?php endif; ?>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-8">
            <div style="" class="alert  alert-light mb-4">
                <?= $product['description']; ?>
            </div>
        </div>
        <div class="col-4">
            <div class="container">
                <div class="row mb-3 alert  alert-light">
                    Статус перевірки:<br><strong>
                        <?php
                        switch ($product['checkin']) {
                            case 'Без перевірки':
                                {
                                    echo 'Наразі перевірки немає';
                                }
                                break;
                            case 'CheckMate Pro':
                                {
                                    echo '<img style="width: 20px" src="/static/images/CheckMate Pro.jpg"> CheckMate Pro';
                                }
                                break;
                            case 'StemCell':
                                {
                                    echo '<img style="width: 20px" src="/static/images/StemCell.jpg">StemCell';
                                }
                                break;

                        } ?>
                    </strong>
                </div>
                <div class="row alert  alert-light mb-1 bottomradius">
                    <span class="mb-2"><strong> Формати:</strong></span><br>
                    <?php
                    if (!empty($product['native_format'])) {
                        echo '<span class="text-bg-secondary blok-format m-1 p-1"><span class="alert alert-info p-0 ps-1 pe-1 nativeformat"><strong> Рідний: </strong></span>' . $format_prog['name'] . ' | ' . $product['render_format'] . '</span>';
                    } else {
                        echo '<span class="text-bg-secondary blok-format m-1"> Формат не вказано | Рендер не вказано </span>';
                    }
                    ?>
                </div>
                <div class="row alert  alert-light mb-1 bottomradius topradius">
                    <?php
                    if (!empty($product['format_program'])) {
                        foreach ($product['format_program'] as $category) {
                            echo '<span class="text-bg-secondary blok-format m-1 ">' . $category . '</span>';
                        }
                    } else {
                        echo '<span class="text-bg-secondary blok-format m-1"> Формати програм не вказані </span>';
                    }
                    ?>
                </div>
                <div class="row alert alert-light mb-3 topradius">
                    <?php
                    if (!empty($product['format_model'])) {
                        foreach ($product['format_model'] as $category) {
                            echo '<span class="text-bg-secondary blok-format m-1 ">' . $category . '</span>';
                        }
                    } else {
                        echo '<span class="text-bg-secondary blok-format m-1"> Формати моделі не вказані </span>';
                    }
                    ?>
                </div>
                <div class="row alert  alert-light mb-1 bottomradius">
                    <span class="name-size"> 3D МОДЕЛЬ:<br></span>
                </div>
                <div class="row alert  alert-light mb-1 topradius bottomradius">
                    <div class="container">
                        <div class="row">
                            <div class="col-2">
                                <div class="p-1 outer">
                                    <div class="TypePolygon"></div>
                                </div>
                            </div>
                            <div class="col-10">
                                <div class="alter">
                                    <div class="row">
                                        <?php echo '<span class="text-bg-secondary blok-format m-1">' . $product['polygons'] . ' Polygons</span>'; ?>
                                        <?php echo '<span class="text-bg-secondary blok-format m-1">' . $product['vertices'] . ' Vertices</span>'; ?>
                                        <span class="text-bg-secondary blok-format m-1">non-overlapping Unwrapped UVs</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row alert  alert-light mb-1 topradius bottomradius">
                    <div class="container">
                        <div class="row">
                            <div class="col-2">
                                <div class="p-1 outer">
                                    <div class="TypeTexture"></div>
                                </div>
                            </div>
                            <div class="col-10">
                                <div class="row">
                                    <?php
                                    if (!empty($product['format_texture'])) {
                                        foreach ($product['format_texture'] as $texture) {
                                            echo '<span class="text-bg-secondary blok-format m-1 ">' . $texture . '</span>';
                                        }
                                    } else {
                                        echo '<span class="text-bg-secondary blok-format m-1"> Формати моделі не вказані </span>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row alert  alert-light mb-3 topradius">
                    <div class="container">
                        <div class="row">
                            <div class="col-6">
                                Код товару: <?= $product['id']; ?>
                            </div>
                            <div class="col-6">
                                <div class="text-end">
                                    <?= $statistics_id['date']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row alert  alert-light mb-3 ">
                    <div class="container">
                        <div class="row">
                            <div class="col-4">
                                <?php if(!empty(models\User::getUserById($product['user_id'])['photo'])):?>
                                    <img style="width: 90px" src="/files/user/<?=\models\User::getUserById($product['user_id'])['photo']?>">
                                <?php else : ?>
                                    <img style="width: 90px" src="/static/images/no-image.jpg" class="img-thumbnail" alt="">
                                <?php endif; ?>

                            </div>
                            <div class="col-8">
                                <div class="outer">
                                    <div class="TypeText">
                                    <span><?=\models\User::getUserById($product['user_id'])['firstname']?> <?=\models\User::getUserById($product['user_id'])['lastname']?></span><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php
if (\models\User::isAdmin() || \models\User::isModerator() || \models\User::isUser()):?>
    <?php if ($product['user_id'] == $_SESSION['user']['id'] || \models\User::isAdmin() || \models\User::isModerator()): ?>
        <div class="row">
            <div class="col-12 text-center">
                <p class="mb-3">
                    <a href="/product/edit/<?= $product['id'] ?>" class="btn btn-primary">Редагувати</a>
                    <a href="/product/delete/<?= $product['id'] ?>" class="btn btn-danger">Видалити</a>
                </p>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<div class="row">
    <div class="col-12 text-center">
        <p class="mt-3 mb-3">
            <a href="/product" class="btn btn-success">Повернутися назад</a>
        </p>
    </div>
</div>
