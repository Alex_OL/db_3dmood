<?php
/** @var array $product */
core\Core::getInstance()->pageParams['title'] = 'Видалення продукту';
?>
<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Видалити продукт "<?=$product['name'] ?>"?</h4>
    <p>Після видалення продукту всі дані товара пропадуть</p>
    <hr>
    <p class="mb-0">
        <a href="/product/delete/<?=$product['id'] ?>/yes" class="btn btn-danger">Видалити</a>
        <a href="/product" class="btn btn-light">Відмінити</a>
    </p>
</div>
