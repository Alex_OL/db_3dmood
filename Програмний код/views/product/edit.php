<?php
/** @var array $category */
/** @var array $model */
/** @var array $errors */
/** @var array $categories */
/** @var array $format_programs */
/** @var array $format_model */
core\Core::getInstance()->pageParams['title'] = 'Редагування товару';

?>
<h2 class="h3 mb-3 fw-normal text-center">Додавання товару</h2>
<form action="" method="post" enctype="multipart/form-data">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label for="name" class="form-label"><strong>Назва товару</strong></label>
                    <input type="text" class="form-control" id="name" name="name" value="<?= $model['name'] ?>"
                           placeholder="">
                    <?php if (!empty($errors['name'])): ?>
                        <div class="form-text text-danger"> <?= $errors['name']; ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <div class="container">
                        <div class="row">
                            <label for="name" class="form-label"><strong>Ціна товару</strong></label>
                            <div class="col-6">
                                <input min="0" type="number" class="form-control" id="price" name="price"
                                       value="<?= $model['price'] ?>" placeholder="">
                            </div>
                            <div class="col-6 p-2">
                                <strong>$</strong>
                            </div>
                        </div>
                        <?php if (!empty($errors['price'])): ?>
                            <div class="form-text text-danger"> <?= $errors['price']; ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label for="name" class="form-label"><strong>Виберіть категорію товару</strong></label>
                    <select class="form-control" id="category_id" name="category_id" placeholder="">
                        <?php foreach ($categories as $category) : ?>
                            <option <? if ($category['id'] == $model['category_id']) echo 'selected' ?>
                                    value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php if (!empty($errors['category_id'])): ?>
                        <div class="form-text text-danger"> <?= $errors['category_id']; ?></div>
                    <?php endif; ?>
                </div>

            </div>
            <div class="col-6">
                <div class="mb-3">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <label for="name" class="form-label"><strong>Сертифікація</strong></label>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="checkin" id="checkin"
                                                       value="Без перевірки" <?php if (!empty($model['checkin'])) {
                                                    if ($model['checkin'] == 'Без перевірки') echo 'checked';
                                                } ?>>

                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    Без перевірки
                                                </label>
                                            </div>

                                        </div>
                                        <div class="col-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="checkin" id="checkin"
                                                       value="CheckMate Pro" <?php if (!empty($model['checkin'])) {
                                                    if ($model['checkin'] == 'CheckMate Pro') echo 'checked';
                                                } ?>>
                                                <label class="form-check-label" for="flexRadioDefault2">
                                                    CheckMate Pro
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="checkin" id="checkin"
                                                       value="StemCell" <?php if (!empty($model['checkin'])) {
                                                    if ($model['checkin'] == 'StemCell')
                                                        echo 'checked';
                                                } ?>>
                                                <label class="form-check-label" for="flexRadioDefault2">
                                                    StemCell
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if (!empty($errors['checkin'])): ?>
                                    <div class="form-text text-danger"> <?= $errors['checkin']; ?></div>
                                <?php endif; ?>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <strong class="h3">Формати</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-6 mb-3">

                <strong>Рідний формат</strong>
                <select class="form-control" id="native_format" name="native_format" placeholder="">
                    <?php foreach ($format_programs as $format): ?>
                        <option class="form-check-input" role="switch" type="checkbox" id="native_format" name="native_format" value="<?= $format['id'] ?>"
                            <?php
                            if (!empty($model['native_format']))
                                if ($format['id'] ==  $model['native_format'])
                                    echo 'selected';
                            ?>
                        >
                            <label class="form-check-label" for="flexSwitchCheckChecked"><?= $format['name'] ?></label>
                        </option>

                    <?php endforeach; ?>
                </select>
                <?php if (!empty($errors['native_format'])): ?>
                    <div class="form-text text-danger"> <?= $errors['native_format']; ?></div>
                <?php endif; ?>
            </div>
            <div class="col-6">
                <strong>Рендер</strong>
                <select class="form-control" id="render_format" name="render_format" placeholder="">
                    <option class="form-check-input" role="switch" type="checkbox" id="render_format"
                            name="render_format[]" value="Corona" <?php if ($model['render_format'] == 'Corona') echo 'selected' ?>>
                        <label class="form-check-label" for="flexSwitchCheckChecked">Corona</label>
                    </option>
                    <option class="form-check-input" role="switch" type="checkbox" id="render_format"
                            name="render_format[]" value="V-Ray" <?php if ($model['render_format'] == 'V-Ray') echo 'selected' ?>>
                        <label class="form-check-label" for="flexSwitchCheckChecked">V-Ray</label>
                    </option>
                    <option class="form-check-input" role="switch" type="checkbox" id="render_format"
                            name="render_format[]" value="Maxwell" <?php if ($model['render_format'] == 'Maxwell') echo 'selected' ?>>
                        <label class="form-check-label" for="flexSwitchCheckChecked">Maxwell</label>
                    </option>
                    <option class="form-check-input" role="switch" type="checkbox" id="render_format"
                            name="render_format[]" value="mental ray" <?php if ($model['render_format'] == 'mental ray') echo 'selected' ?>>
                        <label class="form-check-label" for="flexSwitchCheckChecked">mental ray</label>
                    </option>
                    <option class="form-check-input" role="switch" type="checkbox" id="render_format"
                            name="render_format[]" value="Quicksilver" <?php if ($model['render_format'] == 'Quicksilver') echo 'selected' ?>>
                        <label class="form-check-label" for="flexSwitchCheckChecked">Quicksilver</label>
                    </option>
                    <option class="form-check-input" role="switch" type="checkbox" id="render_format"
                            name="render_format[]" value="Redshift" <?php if ($model['render_format'] == 'Redshift') echo 'selected' ?> >
                        <label class="form-check-label" for="flexSwitchCheckChecked">Redshift</label>
                    </option>
                    <option class="form-check-input" role="switch" type="checkbox" id="render_format"
                            name="render_format[]" value="Krakatoa" <?php if ($model['render_format'] == 'Krakatoa') echo 'selected' ?>>
                        <label class="form-check-label" for="flexSwitchCheckChecked">Krakatoa</label>
                    </option>
                    <option class="form-check-input" role="switch" type="checkbox" id="render_format"
                            name="render_format[]" value="iray" <?php if ($model['render_format'] == 'iray') echo 'selected' ?> >
                        <label class="form-check-label" for="flexSwitchCheckChecked">iray</label>
                    </option>
                    <option class="form-check-input" role="switch" type="checkbox" id="render_format"
                            name="render_format[]" value="Other" <?php if ($model['render_format'] == 'Other') echo 'selected' ?>>
                        <label class="form-check-label" for="flexSwitchCheckChecked">Other</label>
                    </option>
            </div>
            </select>
            <?php if (!empty($errors['render_format'])): ?>
                <div class="form-text text-danger"> <?= $errors['render_format']; ?></div>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="mb-3">
                <label for="name" class="form-label"><strong>Програм</strong></label><br>
                <?php if (!empty($errors['format_program'])): ?>
                    <div class="form-text text-danger"> <?= $errors['format_program']; ?></div>
                <?php endif; ?>
                <?php foreach ($format_programs as $format): ?>
                    <div class="form-check form-switch">
                        <input class="form-check-input" role="switch" type="checkbox" id="format_program"
                               name="format_program[]" value="<?= $format['name'] ?>"
                            <?php if (!empty($model['format_program']))
                                if (in_array($format['name'], $model['format_program']))
                                    echo 'checked';
                            ?>/>
                        <label class="form-check-label" for="flexSwitchCheckChecked"><?= $format['name'] ?></label>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-6">
            <div class="mb-3">
                <label for="name" class="form-label"><strong>Моделі</strong></label><br>
                <?php if (!empty($errors['format_model'])): ?>
                    <div class="form-text text-danger"> <?= $errors['format_model']; ?></div>
                <?php endif; ?>
                <?php foreach ($format_model as $format): ?>
                    <div class="form-check form-switch">
                        <input class="form-check-input" role="switch" type="checkbox" id="format_model"
                               name="format_model[]" value="<?= $format['name'] ?>"
                            <?php if (!empty($model['format_model']))
                                if (in_array($format['name'], $model['format_model']))
                                    echo 'checked';
                            ?>/>
                        <label class="form-check-label" for="flexSwitchCheckChecked"><?= $format['name'] ?></label>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    </div>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label for="name" class="form-label"><strong>Геометрія</strong></label>
                    <select class="form-control" id="geometry" name="geometry" placeholder="">
                        <option <? if ($model['geometry'] == 'Polygonal Quads only') echo 'selected' ?> value="Polygonal Quads only">Polygonal Quads only
                        </option>
                        <option <? if ($model['geometry'] == 'Polygonal Quads/Tris Geometry') echo 'selected' ?> value="Polygonal Quads/Tris Geometry">Polygonal Quads/Tris Geometry
                        </option>
                        <option <? if ($model['geometry'] == 'Polygonal Tris only') echo 'selected' ?> value="Polygonal Tris only">Polygonal Tris only
                        </option>
                        <option <? if ($model['geometry'] == 'Polygonal Ngons used') echo 'selected' ?> value="Polygonal Ngons used">Polygonal Ngons used
                        </option>
                        <option <? if ($model['geometry'] == 'Polygonal') echo 'selected' ?> value="Polygonal">Polygonal
                        </option>
                        <option <? if ($model['geometry'] == 'Subdivision') echo 'selected' ?> value="Subdivision">Subdivision
                        </option>
                    </select>
                    <?php if (!empty($errors['geometry'])): ?>
                        <div class="form-text text-danger"> <?= $errors['geometry']; ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <label for="name" class="form-label"><strong>Кількість полігонів</strong></label>
                    <input min="0" type="number" class="form-control" id="polygons" name="polygons"
                           value="<?= $model['polygons'] ?>" placeholder="">
                    <?php if (!empty($errors['polygons'])): ?>
                        <div class="form-text text-danger"> <?= $errors['polygons']; ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <label for="name" class="form-label"><strong>Кількість точок</strong></label>
                    <input min="0" type="number" class="form-control" id="vertices" name="vertices"
                           value="<?= $model['vertices'] ?>" placeholder="">
                    <?php if (!empty($errors['vertices'])): ?>
                        <div class="form-text text-danger"> <?= $errors['vertices']; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="mb-3">
                <div class="container">
                    <div class="row">
                        <div class="form-check col-2">
                            <input class="form-check-input" type="checkbox" value="Textures" id="format_texture"
                                   name="format_texture[]" <?php
                            if(!empty($model['format_texture']))
                                if(in_array('Textures',$model['format_texture']))
                                    echo 'checked';?>>
                            <label class="form-check-label" for="flexCheckDefault">
                                Textures
                            </label>
                        </div>
                        <div class="form-check col-2">
                            <input class="form-check-input" type="checkbox" value="Materials" id="format_texture"
                                   name="format_texture[]" <?php if(!empty($model['format_texture']))
                                       if( in_array('Materials',$model['format_texture']))
                                           echo 'checked';?> >
                            <label class="form-check-label" for="flexCheckDefault">
                                Materials
                            </label>
                        </div>
                        <div class="form-check col-2">
                            <input class="form-check-input" type="checkbox" value="Rigged" id="format_texture"
                                   name="format_texture[]"<?php if(!empty($model['format_texture']))
                                if(in_array('Rigged',$model['format_texture']))
                                    echo 'checked';?>>
                            <label class="form-check-label" for="flexCheckDefault">
                                Rigged
                            </label>
                        </div>
                        <div class="form-check col-2">
                            <input class="form-check-input" type="checkbox" value="Animated" id="format_texture"
                                   name="format_texture[]"<?php if(!empty($model['format_texture']))
                                if(in_array('Animated',$model['format_texture']))
                                    echo 'checked';?>>
                            <label class="form-check-label" for="flexCheckDefault">
                                Animated
                            </label>
                        </div>
                        <div class="form-check col-2">
                            <input class="form-check-input" type="checkbox" value="UV Mapped" id="format_texture"
                                   name="format_texture[]"<?php if(!empty($model['format_texture']))
                                if(in_array('UV Mapped',$model['format_texture']))
                                    echo 'checked';?>>
                            <label class="form-check-label" for="flexCheckDefault">
                                UV Mapped
                            </label>
                        </div>
                        <?php if (!empty($errors['format_texture'])): ?>
                            <div class="form-text text-danger"> <?= $errors['format_texture']; ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="mb-3">
                <div class="col-2">

                    <label for="name" class="form-label"><strong>Чи відображати товар</strong></label>
                    <select class="form-control text-center" id="visible" name="visible" placeholder="">
                        <option <? if($model['visible']==1) echo 'selected';?>   value="1">так</option>
                        <option <? if($model['visible']==0) echo 'selected';?>   value="0">ні</option>
                    </select>
                    <?php if (!empty($errors['visible'])): ?>
                        <div class="form-text text-danger"> <?= $errors['visible']; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mb-3">
        <label for="file" class="form-label">Файли з фотографією для товару</label>
        <input multiple type="file" class="form-control" name="file[]" id="file" accept="image/jpeg"/>
    </div>

    <div class="mb-3">
        <label for="name" class="form-label"><strong> Опис товару</strong></label>
        <textarea class="ckeditor form-control" name="description"
                  id="description"><?= $model['description'] ?></textarea>
        <?php if (!empty($errors['description'])): ?>
            <div class="form-text text-danger"> <?= $errors['description']; ?></div>
        <?php endif; ?>
    </div>
    <div>
        <button class="btn btn-primary">Редагувати</button>
    </div>
</form>

<script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
<script>
    let editors = document.querySelectorAll('.ckeditor');
    for (let editor of editors) {
        ClassicEditor
            .create(editor)
            .catch(error => {
                console.error(error);
            });
    }
</script>

