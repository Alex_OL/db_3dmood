<?php
/** @var array $products */
/** @var array $user */
core\Core::getInstance()->pageParams['title'] = 'Статистика';
use models\User;
if (User::isAdmin() || User::isModerator() || User::isUser()) : ?>

<div style="position: relative; text-align: center;">
    <h5>Ця сторінка це:</h5><div class="row">Детальна інформація про ваші продукти за весь період часу.
        Використовуйте цей звіт, щоб побачити, які з ваших продуктів переглядають,
        додають до кошика та купують. Ви можете використовувати цю інформацію, щоб покращити презентацію та продажі
        ваших продуктів.</div>
    <h3 class="center mb-3">Статистика товарів</h3>
    <?php
    $N = 1;
    echo "<table class='table table-dark table-sm mb-3' style='font-size: 18px;
    width: 80%;
    margin: 0 auto;'>";
    echo "<thead class='center'>";
    echo "<tr>
                    <th scope='col'>№</th>
                    <th scope='col'>Назва</th>
                    <th scope='col'>Дата виставлення</th>
                    <th scope='col'>Product ID</th>
                    <th scope='col'>Ціна</th>
                    <th scope='col'>К. Переглядів</th>
                    <th scope='col'>К. Покупок</th>
                    <th scope='col'>Виручка</th>
               </tr>";
    foreach ($products as $product) :
        echo "<tr class='center'>";
        echo "<th scope='row'>" . $N . "</th>";
        echo "<td style='text-align: left'><a style='text-decoration: none; color: darkgrey' href='/product/view/" . $product["id"] . "'>" . $product["name"] . "</td>";
        $stat = \models\Statistic::getStatisticById($product['statistics_id']);
        echo "<td>" . $stat["date"] . "</td>";
        echo "<td><a style='text-decoration: none; color: darkgrey' href='/product/view/" . $product["id"] . "'>" . $product["id"] . "</td>";
        echo "<td>" . $product["price"] . "$</td>";
        echo "<td>" . $stat["number_of_views"] . "</td>";
        echo "<td>" . $stat["number_of_purchases"] . "</td>";
        echo "<td>" . $stat["number_of_purchases"] * $product["price"] . "$</td>";
        echo "</tr>";
        $N++;
    endforeach;
    echo "</table>";
    ?>
</div>
<? endif; ?>
