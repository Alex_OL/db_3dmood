<?php
/** @var array $rows */

use core\Utils;
use models\User;

core\Core::getInstance()->pageParams['title'] = 'Пошук';
?>
<style>
    .outer {
        text-align: center;
        height: 100%;
        vertical-align: middle;
        position: relative;

    }
    .textt{
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
    }
    .Buy {
        background: url("/static/images/buy.png") no-repeat;
        background-size: 100%;
        width: 30px;
        height: 30px;
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
    }
</style>
<h1 class="h1 mb-3 fw-normal text-center">Пошук товарів</h1>
<?php if (User::isAdmin() || User::isModerator()) : ?>
    <div class="mb-3">
        <a href="/product/add" class="btn btn-success">Додати товар</a>
    </div>
<?php endif; ?>
<div class="row row-cols-1 row-cols-md-5 g-4 categories-list">
    <?php foreach ($rows as $row) :
        if($row['visible']==1) :
//        if (!empty($row['format_program']))
//            $row['format_program'] = Utils::explodeToArray($row['format_program']);
//        if (!empty($row['format_model']))
//            $row['format_model'] = Utils::explodeToArray($row['format_model']);
//        if (!empty($row['format_texture']))
//            $row['format_texture'] = Utils::explodeToArray($row['format_texture']);
            if (!empty($row['photos']))
                $row['photos'] = Utils::explodeToArray($row['photos']);
            ?>
            <div class="col">
                <a href="/product/view/<?= $row['id'] ?>" class="card-link">
                    <div class="card">
                        <?php $filePath = 'files/product/' . $row['name'] . '/' . $row['photos'][0];


                        ?>
                        <?php if (is_file($filePath)) : ?>
                            <img style="height: 160px" src="/<?= $filePath ?>" class="card-img-top" alt="">
                        <?php else: ?>
                            <img style="height: 160px" src="/static/images/no-image.jpg" class="card-img-top" alt="">
                        <?php endif; ?>
                        <div style="height: 80px" class="card-body ">
                            <div class="row">
                                <div class="col-9">
                                    <div class="outer">
                                        <h5 class="card-title text-center textt"><?= $row['name'] ?></h5>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="outer">
                                        <form action="" method="post">
                                            <input min="0" type="hidden" id="product_id" name="product_id"
                                                   value="<?= $row['id'] ?>">
                                            <button style="width: 50px;height: 50px;border: none;background: transparent;"  type="submit">
                                                <div class="Buy"></div>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if (User::isAdmin() || User::isModerator()) : ?>
                            <div class="card-body text-center">
                                <a href="/product/edit/<?= $row['id'] ?>" class="btn btn-primary m-2">Редагувати</a>
                                <a href="/product/delete/<?= $row['id'] ?>" class="btn btn-danger m-2">Видалити</a>
                            </div>
                        <? endif; ?>
                    </div>
                </a>
            </div>
        <?php
        endif;
    endforeach; ?>
</div>