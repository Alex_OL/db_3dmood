<?php
/** @var array $rows */

use models\User;
core\Core::getInstance()->pageParams['title'] = 'Головна сторінка';
?>
<div class="container">
    <div class="container">
        <h1 class="text-center">3D MOOD</h1>

        <p>
            <strong> 3D MOOD - </strong> це цифрова компанія яка продає різноманітні 3D моделі використані в 3D графіці, для
            різних галузь, таких як комп'ютерні ігри та архітектура.
        </p>
        <p>
            Моделі <i> 3D MOOD </i> використовують розробники ігор, інформаційні агенства, архітекторами, студіями візуальних ефектів,
            рекламщиками та творчими професіоналами по всьому світу.
        </p>
        <p>
            Наша ціль - зекономити час клієнтів на створення відмінної 3D моделі і замість цього дозволити їм витратити час на інші області роботи.
        </p>
        <p>
            Другорядна ціль <i> 3D MOOD </i> складається в тому щоб направити творчий потенціал художників по всьому світу на постійне покращення нашої бібліотеки
            3D моделей, допомогаючи художникам побудувати карєру просійних 3D-моделлерів.
        </p>
    </div>
    <hr>
    <div class="container">
        <h2 class="h1 mb-3 fw-normal text-center">Список категорій</h2>
        <div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
            <?php foreach ($rows as $row) : ?>
                <div class="col">
                    <a href="/category/view/<?= $row['id'] ?>" class="card-link">
                        <div class="card">
                            <?php $filePath = 'files/category/' . $row['photo']; ?>
                            <?php if (is_file($filePath)) : ?>
                                <img style="height: 200px"  src="/<?= $filePath ?>" class="card-img-top" alt="">
                            <?php else: ?>
                                <img style="height: 200px" src="/static/images/no-image.jpg" class="card-img-top" alt="">
                            <?php endif; ?>
                            <div class="card-body">
                                <h5 class="card-title text-center"><?= $row['name'] ?></h5>
                            </div>
                            <div class="card-body">

                            </div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>

