<?php
/** @var array $user */
core\Core::getInstance()->pageParams['title'] = 'Видалення користувача';
?>
<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Видалити користувача "<?=$user['login'] ?>"?</h4>
    <p>Після видалення користувача всі його дані зникнуть</p>
    <hr>
    <p class="mb-0">
        <a href="/user/delete/<?=$user['id'] ?>/yes" class="btn btn-danger">Видалити</a>
        <a href="/user" class="btn btn-light">Відмінити</a>
    </p>
</div>
