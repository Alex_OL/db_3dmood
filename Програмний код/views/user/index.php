<?php
/** @var array $rows */

use models\User;
core\Core::getInstance()->pageParams['title'] = 'Користувачі';
?>
<h2 class="h1 mb-3 fw-normal text-center">Список користуівчів</h2>
<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php foreach ($rows as $row) : ?>
        <div class="col">
            <a href="/user/edit/<?= $row['id'] ?>" class="card-link">
                <div class="card">
                    <?php $filePath = 'files/user/' . $row['photo']; ?>
                    <?php if (is_file($filePath)) : ?>
                        <img style="height: 210px" src="/<?= $filePath ?>" class="card-img-top" alt="">
                    <?php else: ?>
                        <img style="height: 210px" src="/static/images/no-image.jpg" class="card-img-top" alt="">
                    <?php endif; ?>
                    <div class="card-body">
                        <h5 class="card-title text-center"><?= $row['login'] ?></h5>
                    </div>
                    <div class="card-body text-center">
                        <a href="/user/edit/<?= $row['id'] ?>" class="btn btn-primary">Редагувати</a>
                        <a href="/user/delete/<?= $row['id'] ?>" class="btn btn-danger">Видалити</a>
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>