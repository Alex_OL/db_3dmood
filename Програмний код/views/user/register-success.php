<?php
core\Core::getInstance()->pageParams['title'] = 'Успішна реєстрація';
?>
<div>
    <div class="alert alert-success" role="alert">
        Ви успішно зареєструвалися!
    </div>
    <p class="mb-3">
        <a href="/user/login" class="btn btn-success">Продовжити</a>
        <a href="/user/register" class="btn btn-light">Відмінити</a>
    </p>
</div>