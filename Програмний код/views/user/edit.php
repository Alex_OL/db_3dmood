<?php
/** @var array $user */
/** @var array $model */

/** @var array $errors */

use core\Utils;

core\Core::getInstance()->pageParams['title'] = 'Профіль коритувача та редагування';
?>
<h2 class="h1 mb-3 fw-normal text-center">Користувач</h2>
<form action="" method="post" enctype="multipart/form-data">
    <div class="row mb-3">
        <div class="col-6">
            <?php $filePath = 'files/user/' . $user['photo']; ?>
            <?php if (is_file($filePath)) : ?>
                <img class="img-thumbnail card-img-top" src="/<?= $filePath ?>" alt="">
            <?php else: ?>
                <img class="img-thumbnail card-img-top" src="/static/images/no-image.jpg" alt="">
            <?php endif; ?>
            <div class="mb-3">
                <label for="file" class="form-label">Файл з фотографією для категорії (замінити фото)</label>
                <input type="file" class="form-control" name="file" id="file" accept="image/jpeg"/>
            </div>
        </div>
        <div class="col-6 mb-6">
            <div class="mb-12 text-center">
                <label for="name" class="form-label">Логін: <strong><?= $user['login'] ?></strong> </label>
            </div>
            <div class="row mt-5 mb-3">
                <div class="col-6 mb-6">
                    <label for="name" class="form-label">Ім'я</label>
                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder=""
                           value="<?= $user['firstname'] ?>">
                    <?php if (!empty($errors['firstname'])): ?>
                        <div class="form-text text-danger"> <?= $errors['firstname']; ?></div>
                    <?php endif; ?>
                </div>
                <div class="col-6 mb-6">
                    <label for="name" class="form-label">Прізвище</label>
                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder=""
                           value="<?= $user['lastname'] ?>">
                    <?php if (!empty($errors['lastname'])): ?>
                        <div class="form-text text-danger"> <?= $errors['lastname']; ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row mt-5 mb-5">
                <div class="col-6 ">
                    <label for="name" class="form-label">Пароль</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="" value="">
                    <?php if (!empty($errors['password'])): ?>
                        <div class="form-text text-danger"> <?= $errors['password']; ?></div>
                    <?php endif; ?>
                </div>
                <div class="col-6">
                    <label for="name" class="form-label">Пароль(ще раз)</label>
                    <input type="password" class="form-control" id="password2" name="password2" placeholder="" value="">
                </div>
            </div>

            <? if (\models\User::isAdmin()): ?>
                <label for="name" class="form-label">Рівень доступу:</label>
                <select class="form-control" id="access_level" name="access_level" placeholder="">
                    <option class="form-check-input" role="switch" type="checkbox" id="access_level"
                            name="access_level[]" value="10" <?php if ($user['access_level'] == 10) echo 'selected' ?>>
                        <label class="form-check-label" for="flexSwitchCheckChecked">Admin</label>
                    </option>
                    <option class="form-check-input" role="switch" type="checkbox" id="access_level"
                            name="access_level[]" value="5" <?php if ($user['access_level'] == 5) echo 'selected' ?>>
                        <label class="form-check-label" for="flexSwitchCheckChecked">Moderator</label>
                    </option>
                    <option class="form-check-input" role="switch" type="checkbox" id="access_level"
                            name="access_level[]" value="1" <?php if ($user['access_level'] == 1) echo 'selected' ?>>
                        <label class="form-check-label" for="flexSwitchCheckChecked">User</label>
                    </option>
                </select>
            <? else : ?>
                <div class="row">
                    <div class="col-12">
                        <label for="name" class="form-label">Рівень доступу:</label>
                            <strong><?= \models\User::getAccessLevel($user['id']); ?></strong>
                    </div>
                </div>
            <? endif; ?>
            <div class="row">
                <div class="col-12 mt-2">
                    <label for="name" class="form-label">Кількість зароблених грошей:
                        <strong class="link-success"><?=$user['price']?>$</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center">
        <button class="btn btn-primary">Зберегти</button>
        <a href="/" class="btn btn-warning">Повернутися на головну</a>
    </div>
</form>
<hr>
<div class="text-center mb-4">
    <a href="/product/add" class="btn btn-primary">Додати товар</a>
    <a href="/statistic/view" class="btn btn btn-info">Переглянути статистику</a>
</div>
<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php foreach (\models\Product::getProductsInUser($user['id']) as $row) :
        if (!empty($row['format_program']))
            $row['format_program'] = Utils::explodeToArray($row['format_program']);
        if (!empty($row['format_model']))
            $row['format_model'] = Utils::explodeToArray($row['format_model']);
        if (!empty($row['format_texture']))
            $row['format_texture'] = Utils::explodeToArray($row['format_texture']);
        if (!empty($row['photos']))
            $row['photos'] = Utils::explodeToArray($row['photos']);
        ?>
        <div class="col">
            <a href="/product/view/<?= $row['id'] ?>" class="card-link">
                <div class="card">
                    <?php $filePath = 'files/product/' . $row['name'] . '/' . $row['photos'][0];


                    ?>
                    <?php if (is_file($filePath)) : ?>
                        <img style="height: 210px" src="/<?= $filePath ?>" class="card-img-top" alt="">
                    <?php else: ?>
                        <img style="height: 210px" src="/static/images/no-image.jpg" class="card-img-top" alt="">
                    <?php endif; ?>
                    <div class="card-body">
                        <h5 class="card-title text-center"><?= $row['name'] ?></h5>
                    </div>
                    <div class="card-body text-center">
                        <a href="/product/edit/<?= $row['id'] ?>" class="btn btn-primary">Редагувати</a>
                        <a href="/product/delete/<?= $row['id'] ?>" class="btn btn-danger">Видалити</a>
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
