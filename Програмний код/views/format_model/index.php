<?php
/** @var array $format_model */

use models\User;

core\Core::getInstance()->pageParams['title'] = 'Формати програм';
?>

<h2 class="h1 mb-3 fw-normal text-center">Список форматів</h2>
<?php if (User::isAdmin() || User::isModerator()) : ?>
    <div class="mb-3">
        <a href="/format_model/add" class="btn btn-success">Додати Формат</a>
    </div>

<?php endif; ?>

<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php foreach ($format_model as $row) : ?>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center"><?= $row['name'] ?></h5>
                </div>
                <div class="card-body text-center">
                    <a href="/format_model/edit/<?= $row['id'] ?>" class="btn btn-primary">Редагувати</a>
                    <a href="/format_model/delete/<?= $row['id'] ?>" class="btn btn-danger">Видалити</a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>