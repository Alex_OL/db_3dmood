<?php
/** @var array $format_model */
core\Core::getInstance()->pageParams['title'] = 'Видалення формату';
?>
<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Видалити категорію "<?=$format_model['name'] ?>"?</h4>
    <p>Після видалення формату всі товари даного формату потраплять до стандартної категорії <b>"Не визначено"</b></p>
    <hr>
    <p class="mb-0">
        <a href="/format_model/delete/<?=$format_model['id'] ?>/yes" class="btn btn-danger">Видалити</a>
        <a href="/format_model" class="btn btn-light">Відмінити</a>
    </p>
</div>
