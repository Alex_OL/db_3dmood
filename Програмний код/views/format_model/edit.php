<?php
/** @var array $format_model */
/** @var array $model */
/** @var array $errors */
core\Core::getInstance()->pageParams['title'] = 'Редагування формату';
?>
<h2 class="h1 mb-3 fw-normal text-center">Редагування формату моделі</h2>
<form action="" method="post" enctype="multipart/form-data">
    <div class="mb-3">
        <div class="container">
            <div class="row">
                <div class="col-4 mb-3">
                    <label for="name" class="form-label">Назва формату</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder=""
                           value="<?= $format_model['name'] ?>">
                    <?php if (!empty($errors['name'])): ?>
                        <div class="form-text text-danger"> <?= $errors['name']; ?></div>
                    <?php endif; ?>
                </div>
                <div>
                    <button class="btn btn-primary">Зберегти</button>
                    <a href="/format_model" class="btn btn-light">Відмінити</a>
                </div>
            </div>
        </div>
    </div>
</form>