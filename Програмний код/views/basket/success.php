<?php
core\Core::getInstance()->pageParams['title'] = 'Успішна покупка';
?>
<div class="text-center">
    <div class="alert alert-success" role="alert">
        Товар успішно придбано!
    </div>
    <p class="mb-0">
        <a href="/" class="btn btn-outline-info">На головну</a>
    </p>
</div>