<?php
/** @var array $basket */
/** @var array $errors */
?>

<?php
if (!empty($basket['products'])):
    ?>
    <style>
        .outer {
            text-align: center;
            width: 100%;
            vertical-align: middle;
            position: relative;

        }

        .center {
            position: fixed;
            left: 50%;
            transform: translateX(-50%);
            margin-bottom: 3px;
        }
    </style>
    <h1 class="h3 mb-3 fw-normal text-center">Кошик</h1>
    <div class="row">
        <div class="col-2">
            <a href="/product" class="btn btn-outline-warning mb-3">Додати товар</a>
        </div>
        <div class="col-2">

            <form style="" class="text-center text-first" method="post" action="">
                <button type="submit" name="delAll" class="btn btn-outline-danger">Очистити кошик</button>
            </form>
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>№</th>
            <th>Назва товару</th>
            <th>Вартість</th>
            <th class="text-center">Видалення</th>
        </tr>
        </thead>
        <?php
        $index = 1;
        foreach ($basket['products'] as $row): ?>
            <tr>
                <td><?= $index ?></td>
                <td><?= $row['product']['name'] ?></td>
                <td><?= $row['product']['price'] ?>$</td>
                <td>
                    <form action="" method="post" class="text-center">
                        <input min="0" type="hidden" id="product_id" name="product_id"
                               value="<?= $row['product']['id'] ?>">
                        <button class="btn btn-outline-danger" name="del" type="submit">Видалити</button>
                    </form>
                </td>
            </tr>
            <?
            $index++;
        endforeach; ?>
        <tfoot>
        <th></th>
        <th>Загальна сума:</th>
        <th><?= $basket['total_price'] ?>$</th>
        <th></th>
        </tfoot>
    </table>
    <div class="text-center mb-5">
        <form class="text-center outer" method="post" action="">
            <div style="width: 300px; height: auto; " class="center">
                <?php if (!empty($errors['number_of_card'])): ?>
                    <div class="form-text text-danger mb-3"> <?= $errors['number_of_card']; ?></div>
                <?php endif; ?>
            <input  type="number" class="form-control" id="number_of_card"
                   name="number_of_card" placeholder="">
                <button style="margin-top: 200px" type="submit" name="success" class="btn btn-success mt-2">Придбати</button>
            </div>

            <!--<a href="/basket/success" class="btn btn-success ">Придбати</a>-->
        </form>
    </div>
<div style="height: 20px" class="mb-5"></div>
<span class="mb-5"></span>
<?php else : ?>
    <div class="h1 text-center mb-4"> Кошик порожній!</div>
    <div class="alert alert-info pb-4 pt-4"> Бажаєте додати товари? <a href="/product" class="btn btn-success ">Додати
            товар</a>
    </div>
<?php endif; ?>
