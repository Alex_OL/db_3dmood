<?php
/** @var array $model */
/** @var array $errors */
core\Core::getInstance()->pageParams['title'] = 'Додавання формату для програм';
?>
<h2 class="h1 mb-3 fw-normal text-center">Додавання формат</h2>
<form action="" method="post" enctype="multipart/form-data">
    <div class="mb-3">

        <div class="container">
            <div class="row">
                <div class="col-6 mb-3">
                    <label for="name" class="form-label">Назва формату</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="">
                    <?php if (!empty($errors['name'])): ?>
                        <div class="form-text text-danger "> <?= $errors['name']; ?></div>
                    <?php endif; ?>
                </div>
                <div>
                    <button class="btn btn-primary">Додати</button>
                    <a href="/format_program/index" class="btn btn-outline-danger">Відмінити</a>
                </div>

            </div>
        </div>

    </div>

</form>