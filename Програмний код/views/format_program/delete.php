<?php
/** @var array $format_program */
core\Core::getInstance()->pageParams['title'] = 'Видалення формату';
?>
<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Видалити категорію "<?=$format_program['name'] ?>"?</h4>
    <p>Після видалення формату всі товари даного формату потраплять до стандартної категорії <b>"Не визначено"</b></p>
    <hr>
    <p class="mb-0">
        <a href="/format_program/delete/<?=$format_program['id'] ?>/yes" class="btn btn-danger">Видалити</a>
        <a href="/format_program" class="btn btn-light">Відмінити</a>
    </p>
</div>
