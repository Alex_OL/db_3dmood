<?php
/** @var array $rows */

use models\User;
core\Core::getInstance()->pageParams['title'] = 'Категорії';
?>
<style>
</style>
<h2 class="h1 mb-3 fw-normal text-center">Список категорій</h2>
<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php foreach ($rows as $row) : ?>
        <div class="col">
            <a href="/category/view/<?= $row['id'] ?>" class="card-link">
                <div class="card">
                    <?php $filePath = 'files/category/' . $row['photo']; ?>
                    <?php if (is_file($filePath)) : ?>
                        <img style="height: 200px"  src="/<?= $filePath ?>" class="card-img-top" alt="">
                    <?php else: ?>
                        <img style="height: 200px" src="/static/images/no-image.jpg" class="card-img-top" alt="">
                    <?php endif; ?>
                    <div class="card-body">
                        <h5 class="card-title text-center"><?= $row['name'] ?></h5>
                    </div>
                    <div class="card-body">

                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
