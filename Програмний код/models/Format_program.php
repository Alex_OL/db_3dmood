<?php

namespace models;

use core\Core;
use core\Utils;

class Format_program
{
    protected static $tableName = 'format_program';
    public static function addFormat($name)
    {
        Core::getInstance()->db->insert(self::$tableName, [
            'name' => $name
        ]);
    }

    public static function getFormatById($id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', [
            'id' => $id
        ]);
        if (!empty($rows))
            return $rows[0];
        else
            return null;
    }

    public static function deleteFormat($id)
    {
        Core::getInstance()->db->delete(self::$tableName,
            [
                'id' => $id
            ]);
    }

    public static function updateFormat($id, $newName)
    {
        Core::getInstance()->db->update(self::$tableName, [
            'name' => $newName
        ], [
            'id' => $id
        ]);
    }

    public static function getFormat()
    {
        $rows = Core::getInstance()->db->select(self::$tableName);
        return $rows;
    }
}