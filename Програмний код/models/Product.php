<?php

namespace models;

use core\Core;
use core\Utils;

class Product
{
    protected static $tableName = 'product';

    public static function addProduct($row, $photoPath)
    {

        $dataaddproduct = date('d') . '.' . date('m') . '.' . date('Y');
        Statistic::addStatistic($dataaddproduct);
        $static = Statistic::getStatistic();
        $row["statistics_id"] = end($static)["id"];

        if (!empty($photoPath)) {
            $currentpath = 'files/product/' . $row['name'];
            if (is_dir($currentpath)) {

            } else
                mkdir($currentpath, 0777, true);


            if (is_array($photoPath)) {
                foreach ($photoPath as $foto) {
                    do {
                        $fileName = uniqid() . '.jpg';
                        $row['photos'] [] = $fileName;
                        $newPath = "files/product/{$row['name']}/{$fileName}";
                    } while (file_exists($newPath));
                    move_uploaded_file($foto, $newPath);
                }
            } elseif (is_string($photoPath)) {
                do {
                    $fileName = uniqid() . '.jpg';
                    $row['photos'] [] = $fileName;
                    $newPath = "files/product/{$row['name']}/{$fileName}";
                } while (file_exists($newPath));
                move_uploaded_file($photoPath, $newPath);
            }

            /*do {
                $fileName = uniqid() . '.jpg';
                $row['photos'] [] = $fileName;
                $newPath = "files/product/{$row['name']}/{$fileName}";
            } while (file_exists($newPath));
            move_uploaded_file($photoPath, $newPath);*/

        }

        if (is_array($row['format_program']))
            $row['format_program'] = implode(' ~/!\~ ', $row['format_program']);
        if (is_array($row['format_model']))
            $row['format_model'] = implode(' ~/!\~ ', $row['format_model']);
        if (is_array($row['format_texture']))
            $row['format_texture'] = implode(' ~/!\~ ', $row['format_texture']);
        if (!empty($row['photos']))
            if (is_array($row['photos']))
                $row['photos'] = implode(' ~/!\~ ', $row['photos']);

        if (empty($row['photos']))
            $fieldsList = ['name', 'category_id', 'price', 'short_description', 'description', 'visible', 'checkin', 'format_program', 'format_model', 'polygons', 'vertices', 'format_texture', 'native_format', 'render_format', 'geometry', 'user_id', 'statistics_id'];
        else
            $fieldsList = ['name', 'category_id', 'price', 'short_description', 'description', 'visible', 'checkin', 'format_program', 'format_model', 'polygons', 'vertices', 'format_texture', 'native_format', 'render_format', 'geometry', 'photos', 'user_id', 'statistics_id'];

        $row = Utils::filterArray($row, $fieldsList);
        Core::getInstance()->db->insert(self::$tableName, $row);
    }

    public static function deleteProduct($id)
    {
        self::deletePhotoFile($id);
        Core::getInstance()->db->delete(self::$tableName, [
            'id' => $id
        ]);
    }

    public static function updateProduct($id, $row)
    {
        if (is_array($row['format_program']))
            $row['format_program'] = implode(' ~/!\~ ', $row['format_program']);
        if (is_array($row['format_model']))
            $row['format_model'] = implode(' ~/!\~ ', $row['format_model']);
        if (is_array($row['format_texture']))
            $row['format_texture'] = implode(' ~/!\~ ', $row['format_texture']);
/*        var_dump($row['photos']);
        die;*/

        if (!empty($row['photos']))
            if (is_array($row['photos']))
                $row['photos'] = implode(' ~/!\~ ', $row['photos']);

        if (empty($row['photos']))
            $fieldsList = ['name', 'category_id', 'price', 'short_description', 'description', 'visible', 'checkin', 'format_program', 'format_model', 'polygons', 'vertices', 'format_texture', 'native_format', 'render_format', 'geometry', 'user_id', 'statistics_id'];
        else
            $fieldsList = ['name', 'category_id', 'price', 'short_description', 'description', 'visible', 'checkin', 'format_program', 'format_model', 'polygons', 'vertices', 'format_texture', 'native_format', 'render_format', 'geometry', 'photos', 'user_id', 'statistics_id'];        $row = Utils::filterArray($row, $fieldsList);
        Core::getInstance()->db->update(self::$tableName, $row, [
            'id' => $id
        ]);
    }

    public static function getProductById($id)
    {
        $row = Core::getInstance()->db->select(self::$tableName, '*', [
            'id' => $id
        ]);
        if (!empty($row))
            return $row[0];
        else
            return null;
    }

    public static function getProduct()
    {
        $rows = Core::getInstance()->db->select(self::$tableName);
        return $rows;
    }

    public static function getProductsInCategory($category_id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', [
            'category_id' => $category_id
        ]);
        return $rows;
    }

    public static function getProductsByName($name)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', [
            'name' => $name
        ], false);
        return $rows;
    }


    public static function getProductsInUser($user_id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', [
            'user_id' => $user_id
        ]);
        return $rows;
    }

    public static function deletePhotoFile($id)
    {
        $row = self::getProductById($id);
        $row['photos'] = explode(' ~/!\~ ', $row['photos']);
        if (!empty($row['photos'])) {
            if (is_array($row['photos']))
                foreach ($row['photos'] as $foto) {
                    $photoPath = 'files/product/' . $row['name'] . '/' . $foto;
                    if (is_file($photoPath))
                        unlink($photoPath);
                }
            if (is_string($row['photos'])) {
                $photoPath = 'files/product/' . $row['name'] . '/' . $row['photos'];
                if (is_file($photoPath))
                    unlink($photoPath);
            }
        }
    }

    public static function changePhoto($id, $newPhoto)
    {
        self::deletePhotoFile($id);
        $product = Product::getProductById($id);
        if (is_array($newPhoto)) {
            foreach ($newPhoto as $foto) {
                do {
                    $fileName = uniqid() . '.jpg';
                    $PhotoToDB [] = $fileName;
                    $newPath = "files/product/{$product['name']}/{$fileName}";
                } while (file_exists($newPath));
                move_uploaded_file($foto, $newPath);
            }
        } elseif (is_string($newPhoto)) {
            do {
                $fileName = uniqid() . '.jpg';
                $PhotoToDB [] = $fileName;
                $newPath = "files/product/{$product['name']}/{$fileName}";
            } while (file_exists($newPath));
            move_uploaded_file($newPhoto, $newPath);
        }
        if (is_array($PhotoToDB))
            $PhotoToDB = implode(' ~/!\~ ', $PhotoToDB);

        Core::getInstance()->db->update(self::$tableName, [
            'photos' => $PhotoToDB
        ], [
            'id' => $id
        ]);
    }

    public static function recursiveRemoveDir($dir)
    {

        $includes = glob($dir . '/*');

        foreach ($includes as $include) {

            if (is_dir($include)) {

                recursiveRemoveDir($include);
            } else {

                unlink($include);
            }
        }

        rmdir($dir);
    }

}