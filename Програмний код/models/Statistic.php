<?php

namespace models;

use core\Core;
use core\Utils;

class Statistic
{
    protected static $tableName = 'product_statistics';

    public static function addStatistic($date,$number_of_views = 0,$number_of_purchases = 0)
    {
        Core::getInstance()->db->insert(self::$tableName, [
            'date' => $date,
            'number_of_views'=>$number_of_views,
            'number_of_purchases'=> $number_of_purchases
        ]);
    }

    public static function deleteStatistic($id)
    {
        Core::getInstance()->db->delete(self::$tableName,
            [
                'id' => $id
            ]);
    }


    public static function updateStatistic($id, $row)//$date,$number_of_views=0,$number_of_purchases=0)
    {
//        [
//            'date' => $date,
//            'number_of_views'=>$number_of_views,
//            'number_of_purchases'=> $number_of_purchases
//        ]
        Core::getInstance()->db->update(self::$tableName, $row, [
            'id' => $id
        ]);
    }

    public static function getStatisticById($id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', [
            'id' => $id
        ]);
        if (!empty($rows))
            return $rows[0];
        else
            return null;
    }

    public static function getStatistic()
    {
        $rows = Core::getInstance()->db->select(self::$tableName);
        return $rows;
    }

}