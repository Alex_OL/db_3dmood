<?php

namespace models;

use core\Core;
use core\Utils;

class User
{
    protected static $tableName = 'user';

    public static function addUser($login, $password, $lastname, $firstname)
    {
        \core\Core::getInstance()->db->insert(
            self::$tableName, [
                'login' => $login,
                'password' => self::hashPassword($password),
                'lastname' => $lastname,
                'firstname' => $firstname
            ]
        );
    }
    public static function hashPassword($password) {
        return md5($password);
    }
    public static function updateUser($id, $updatesArray)
    {
        $updatesArray = Utils::filterArray($updatesArray, ['lastname', 'firstname']);
        \core\Core::getInstance()->db->update(self::$tableName,
            $updatesArray, [
                'id' => $id
            ]);
    }

    public static function UpdateUsers($id, $updatesArray)
    {
        $updatesArray['password'] = self::hashPassword($updatesArray['password']);
        $updatesArray = Utils::filterArray($updatesArray, ['lastname', 'firstname','password','photo','access_level']);
        \core\Core::getInstance()->db->update(self::$tableName,
            $updatesArray, [
                'id' => $id
            ]);
    }
    public static function UpdatePrice($id, $updatesArray)
    {
        $updatesArray = Utils::filterArray($updatesArray, ['price']);
        \core\Core::getInstance()->db->update(self::$tableName,
            $updatesArray, [
                'id' => $id
            ]);
    }
    public static function UpdateUsersnotPassword($id, $updatesArray)
    {
        $updatesArray = Utils::filterArray($updatesArray, ['lastname', 'firstname','photo','access_level']);
        \core\Core::getInstance()->db->update(self::$tableName,
            $updatesArray, [
                'id' => $id
            ]);
    }
    public static function isLoginExists($login) {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', [
           'login' => $login
        ]);
        return !empty($user);
    }
    public static function verifyUser($login, $password) {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', [
            'login' => $login,
            'password' => $password
        ]);
        return !empty($user);
    }
    public static function getUserByLoginAndPassword($login, $password) {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', [
            'login' => $login,
            'password' => self::hashPassword($password)
        ]);
        if (!empty($user))
            return $user[0];
        return null;
    }
    public static function authenticateUser($user) {
        $_SESSION['user'] = $user;
    }
    public static function logoutUser() {
        unset($_SESSION['user']);
    }
    public static function isUserAuthenticated() {
        return isset($_SESSION['user']);
    }
    public static function getCurrentAuthenticatedUser() {
        return $_SESSION['user'];
    }

    public static function getUsers()
    {
        $rows = Core::getInstance()->db->select(self::$tableName);
        return $rows;
    }
    public static function getUserById($id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', [
            'id' => $id
        ]);
        if (!empty($rows))
            return $rows[0];
        else
            return null;
    }

    public static function isAdmin() {
        $user = self::getCurrentAuthenticatedUser();
        return $user['access_level'] == 10;
    }
    public static function isModerator() {
        $user = self::getCurrentAuthenticatedUser();
        return $user['access_level'] == 5;
    }
    public static function isUser() {
        $user = self::getCurrentAuthenticatedUser();
        return $user['access_level'] == 1;
    }

    public static function getAccessLevel($id){
        $user = self::getUserById($id);
        switch ($user['access_level']){
            case 1:{
                return 'Звичайний користувач';
            }break;
            case 5:{
                return 'Модератор';
            }break;
            case 10:{
                return 'Адмін';
            }break;
            default:{
                return 'Незазначено';
            }
        }
    }

    public static function deletePhotoFile($id){
        $row = self::getUserById($id);
        $photoPath = 'files/user/' . $row['photo'];
        if (is_file($photoPath))
            unlink($photoPath);
    }
    public static function changePhoto($id, $newPhoto)
    {
        self::deletePhotoFile($id);
        do {
            $fileName = uniqid() . '.jpg';
            $newPath = "files/user/{$fileName}";
        } while (file_exists($newPath));
        move_uploaded_file($newPhoto, $newPath);
        Core::getInstance()->db->update(self::$tableName, [
            'photo' => $fileName
        ], [
            'id' => $id
        ]);
    }
    public static function deleteUser($id)
    {
        self::deletePhotoFile($id);
        Core::getInstance()->db->delete(self::$tableName,
            [
                'id' => $id
            ]);
    }
}