<?php
namespace controllers;

use core\Core;
use models\Basket;
use models\Statistic;
use models\User;

class BasketController extends \core\Controller
{

    public function indexAction()
    {

        $basket = Basket::getProductsInBasket();
//        echo "<pre>";
//        var_dump($_SESSION['basket']);
//        var_dump($basket);
//        die;


        if (Core::getInstance()->requestMethod === 'POST') {
            $errors = [];
            if(isset($_POST['delAll'])) {
                unset($_SESSION['basket']);
                unset($basket);
            }
            if(isset($_POST['del'])) {
                if (empty($_POST['product_id']))
                    $errors['product_id'] = 'Невірний ID продукту!';
            }
            if(isset($_POST['success'])) {
                if (empty($_POST['number_of_card']))
                    $errors['number_of_card'] = 'Не введено номер карти!';
                if (strlen($_POST['number_of_card']) != 16)
                    $errors['number_of_card'] = 'Некоректно введено номер карти!';
            }
            if (empty($errors)) {
                if(isset($_POST['success'])){
                    foreach ($basket['products'] as $tovar) {
                        $stat = Statistic::getStatisticById($tovar['product']["statistics_id"]);
                        $stat["number_of_purchases"]++;
                        Statistic::updateStatistic($stat['id'],$stat);
                    }
                    return $this->redirect('/basket/success');
                }
                if(isset($_POST['del'])) {
                    foreach ($basket as $product)
                        if ($product['id'] == $_POST['product_id']) {
                            unset($basket[$_POST['product_id']]);
                            break;
                        }
                    foreach ($_SESSION['basket'] as $id => $count)
                        if ($id == $_POST['product_id']) {
                            unset($_SESSION['basket'][$id]);
                            break;
                        }
                }
                return $this->redirect('/basket');
            } else {
                return $this->render(null, [
                    'errors' => $errors,
                    'basket' => $basket
                ]);
            }
            return $this->redirect('/basket');
        }

        return $this->render(null, [
            'basket' => $basket
        ]);
    }


    public function successAction()
    {
            $basket = Basket::getProductsInBasket();

            foreach ($basket['products'] as $prod) {
                /*$prod['product']['name'];
                var_dump(file_get_contents('files/product/cacacacaca'));
                die;
                for ($i = 0; $i < count($img; $i++) {
                    $name = explode('/', $img[$i];
                    $name = end($name);
                    file_put_contents($path.'/'.$name, file_get_contents($img[$i]));
                }
               echo 'sdsdsd';
                die;*/
                $user = User::getUserById($prod['product']['user_id']);
                $user['price'] = intval($user['price']) + intval($prod['product']['price']);
                User::UpdatePrice($user['id'], $user);
            }
            unset($_SESSION['basket']);
            unset($basket['products']['product']);
            return $this->render();

    }
}