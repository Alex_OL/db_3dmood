<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Format_model;
use models\User;

class Format_modelController extends Controller
{
    public function indexAction()
    {
        if (!User::isAdmin())
            if (!User::isModerator())
                return $this->error(403);
        $format = Format_model::getFormat();
        return $this->render(null,
            [
                'format_model' => $format
            ]);
    }

    public function addAction()
    {
        if (!User::isAdmin())
            if (!User::isModerator())
                return $this->error(403);
        if (Core::getInstance()->requestMethod === 'POST') {
            $_POST['name'] = trim($_POST['name']);
            $errors = [];
            if (empty($_POST['name']))
                $errors['name'] = 'Назва формату не вказана';
            if (empty($errors)) {
                Format_model::addFormat($_POST['name']);
                return $this->redirect("/format_model");
            } else {
                $model = $_POST;
                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model
                ]);
            }
        }
        return $this->render();
    }

    public function deleteAction($params)
    {
        $id = intval($params[0]);
        $yes = boolval($params[1] === 'yes');
        if (!User::isAdmin())
            if (!User::isModerator())
                return $this->error(403);
        if ($id > 0) {
            $format = Format_model::getFormatById($id);
            if ($yes) {
                Format_model::deleteFormat($id);
                return $this->redirect('/format_model/index');
            }
            return $this->render(null, [
                'format_model' => $format
            ]);
        } else
            return $this->error(403);
    }

    public function editAction($params)
    {
        $id = intval($params[0]);
        if (!User::isAdmin())
            if (!User::isModerator())
                return $this->error(403);
        if ($id > 0) {
            $format_model = Format_model::getFormatById($id);
            if (Core::getInstance()->requestMethod === 'POST') {
                $_POST['name'] = trim($_POST['name']);
                $errors = [];
                if (empty($_POST['name']))
                    $errors['name'] = 'Назва формату невказана';
                if (empty($errors)) {
                    Format_model::updateFormat($id, $_POST['name']);
                    return $this->redirect('/format_model/index');
                } else {
                    $model = $_POST;
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'format_model' => $format_model
                    ]);
                }
            }
            return $this->render(null, [
                'format_model' => $format_model
            ]);
        } else
            return $this->error(403);
    }
}