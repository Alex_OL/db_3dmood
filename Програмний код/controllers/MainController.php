<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Category;
use models\Product;

class MainController extends Controller
{
    public function indexAction()
    {
        $rows = Category::getCategories();
        return $this->render(null,
            [
                'rows' => $rows
            ]);
    }

    public function errorAction($code)
    {
        switch ($code) {
            case 404:
                return $this->render('views/main/error-404.php');
                break;
            case 403:
                return $this->render('views/main/error-403.php');
                break;
        }
    }

    public function searchAction()
    {

        $name = "";
        if (Core::getInstance()->requestMethod === 'GET') {
            if(isset($_GET['query'])){
                if(empty($_GET['query']))
                    return $this->redirect('/');

                $name = "%" . $_GET['query'] . "%";
            }
        }

        $product = Product::getProductsByName($name);

        return $this->render(null,
            [
                'rows' => $product
            ]);

    }
}