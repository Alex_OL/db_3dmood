<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Format_program;
use models\User;

class Format_programController extends Controller
{
    public function indexAction()
    {
        if (!User::isAdmin())
            if (!User::isModerator())
                return $this->error(403);
        $format = Format_program::getFormat();
        return $this->render(null,
            [
                'format_program' => $format
            ]);
    }

    public function addAction()
    {
        if (!User::isAdmin())
            if (!User::isModerator())
                return $this->error(403);
        if (Core::getInstance()->requestMethod === 'POST') {
            $_POST['name'] = trim($_POST['name']);
            $errors = [];
            if (empty($_POST['name']))
                $errors['name'] = 'Назва формату не вказана';
            if (empty($errors)) {
                Format_program::addFormat($_POST['name']);
                return $this->redirect("/format_program");
            } else {
                $model = $_POST;
                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model
                ]);
            }
        }
        return $this->render();
    }

    public function deleteAction($params)
    {
        $id = intval($params[0]);
        $yes = boolval($params[1] === 'yes');
        if (!User::isAdmin())
            if (!User::isModerator())
                return $this->error(403);
        if ($id > 0) {
            $format = Format_program::getFormatById($id);
            if ($yes) {
                Format_program::deleteFormat($id);
                return $this->redirect('/format_program/index');
            }
            return $this->render(null, [
                'format_program' => $format
            ]);
        } else
            return $this->error(403);
    }

    public function editAction($params)
    {
        $id = intval($params[0]);
        if (!User::isAdmin())
            if (!User::isModerator())
                return $this->error(403);
        if ($id > 0) {
            $format_program = Format_program::getFormatById($id);
            if (Core::getInstance()->requestMethod === 'POST') {
                $_POST['name'] = trim($_POST['name']);
                $errors = [];
                if (empty($_POST['name']))
                    $errors['name'] = 'Назва формату невказана';
                if (empty($errors)) {
                    Format_program::updateFormat($id, $_POST['name']);
                    return $this->redirect('/format_program/index');
                } else {
                    $model = $_POST;
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'format_program' => $format_program
                    ]);
                }
            }
            return $this->render(null, [
                'format_program' => $format_program
            ]);
        } else
            return $this->error(403);
    }
}