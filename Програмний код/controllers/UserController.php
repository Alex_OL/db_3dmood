<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\User;

class UserController extends Controller
{
    public function indexAction()
    {
        if (!User::isAdmin())
            return $this->error(403);
        $row = User::getUsers();
        return $this->render(null, [
            'rows' => $row
        ]);
    }

    public function registerAction()
    {
        if (User::isUserAuthenticated())
            $this->redirect('/');
        if (Core::getInstance()->requestMethod === 'POST') {
            $errors = [];
            if (!filter_var($_POST['login'], FILTER_VALIDATE_EMAIL))
                $errors['login'] = 'Помилка при введенні електронної пошти';
            if (User::isLoginExists($_POST['login']))
                $errors['login'] = 'Даний логін зайнято';
            if(empty($_POST['firstname']))
                $errors['firstname'] = "Некоректно вказане ім'я";
            if(empty($_POST['lastname']))
                $errors['lastname'] = "Некоректно вказане прізвище";

            if ($_POST['password'] != $_POST['password2'])
                $errors['password'] = 'Паролі не співпадають';

            if(strlen($_POST['password'])<8 || strlen($_POST['password2'])<8)
                $errors['password'] = 'Пароль занадто короткий';

            if (count($errors) > 0) {
                $model = $_POST;
                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model
                ]);
            } else {
                User::addUser($_POST['login'], $_POST['password'], $_POST['lastname'], $_POST['firstname']);
                return $this->renderView('register-success');
            }

        } else
            return $this->render();
    }

    public function loginAction()
    {
        if (User::isUserAuthenticated())
            $this->redirect('/');
        if (Core::getInstance()->requestMethod === 'POST') {
            $user = User::getUserByLoginAndPassword($_POST['login'], $_POST['password']);
            $error = null;
            if (empty($user)) {
                $error = 'Неправильний логін або пароль';
            } else {
                User::authenticateUser($user);
                $this->redirect('/');
            }
        }
        return $this->render(null, [
            'error' => $error
        ]);
    }
    public function logoutAction() {
        User::logoutUser();
        $this->redirect('/user/login');
    }

    public function editAction($params) {


        $id = intval($params[0]);
        if(empty($_SESSION['user']))
            return $this->error(403);
        if($id!=$_SESSION['user']['id'])
            if(!User::isAdmin())
                return $this->error(403);

        if ($id > 0) {
            $user = User::getUserById($id);
            if (Core::getInstance()->requestMethod === 'POST') {
                $_POST['login'] = trim($_POST['login']);
                $errors = [];
                if(empty($_POST['firstname']))
                    $errors['firstname'] = "Невкоректно вказане ім'я";
                if(empty($_POST['lastname']))
                    $errors['lastname'] = "Невкоректно вказане прізвище";
                if ($_POST['password'] != $_POST['password2'])
                    $errors['password'] = 'Паролі не співпадають';
                /*if (empty($_POST['password']) || empty($_POST['password2']))
                    $errors['password'] = 'Паролі не співпадають';*/
                if((strlen($_POST['password'])<8 && strlen($_POST['password'])>1) || (strlen($_POST['password2'])<8 && strlen($_POST['password2'])>1))
                    $errors['password'] = 'Паролі не співпадають';
                    /*        echo "<pre>";
                var_dump($_POST);
                var_dump($_FILES['file']['tmp_name']);
                die;*/

                if(empty($errors)){
                    $_POST['login'] = $user['login'];
                    if(!empty($_POST['password']))
                        User::UpdateUsers($id, $_POST);
                    if(empty($_POST['password']))
                        User::UpdateUsersnotPassword($id, $_POST);
                    if (!empty($_FILES['file']['tmp_name']))
                        User::changePhoto($id, $_FILES['file']['tmp_name']);
                    if(User::isAdmin())
                    return $this->redirect('/user/index');
                    else
                        return $this->redirect("/");
                } else {
                    $model = $_POST;
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'user' => $user
                    ]);
                }
            }
            return $this->render(null, [
                'user' => $user
            ]);
        } else
            return $this->error(403);
    }
    public function deleteAction($params)
    {
        $id = intval($params[0]);
        $yes = boolval($params[1] === 'yes');
        if (!User::isAdmin())
            return $this->error(403);
        if ($id > 0) {
            $user = User::getUserById($id);
            if ($yes) {
                $filePath = 'files/user/' . $user['photo'];
                if (is_file($filePath))
                    unlink($filePath);
                User::deleteUser($id);
                return $this->redirect('/user/index');
            }
            return $this->render(null, [
                'user' => $user
            ]);
        } else
            return $this->error(403);
    }
}