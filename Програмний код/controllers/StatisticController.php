<?php

namespace controllers;

use core\Core;
use models\Product;
use models\Statistic;
use models\User;

class StatisticController extends \core\Controller
{

    public function indexAction()
    {
        if (!User::isAdmin())
            if (!User::isModerator())
                return $this->error(403);
        $products = Product::getProduct();
        //$statistic = Statistic::getStatistic();
        return $this->render(null,
            [
                'products' => $products,
                //'statisticAll' => $statistic
            ]);
    }
    public function viewAction()
    {
        if (!User::isAdmin())
            if (!User::isModerator())
                if(!User::isUser())
                    return $this->error(403);

        $user = User::getUserById($_SESSION["user"]["id"]);
        $products = Product::getProductsInUser($user["id"]);

        return $this->render(null,
            [
                'products' => $products,
                'user' => $user,
            ]);
    }
}