<?php

namespace controllers;

use Cassandra\Date;
use core\Core;
use core\Utils;
use models\Basket;
use models\Category;
use models\Format_model;
use models\Format_program;
use models\Product;
use models\Statistic;
use models\User;

class ProductController extends \core\Controller
{
    public function indexAction()
    {

        if (Core::getInstance()->requestMethod === 'POST') {
            Basket::addProduct($_POST['product_id']);
        }


        $row = Product::getProduct();
        return $this->render(null, [
            'rows' => $row
        ]);
    }

    public function addAction($params)
    {
        if (!User::isAdmin())
            if (!User::isModerator())
                if (!User::isUser())
                    return $this->error(403);
        $category_id = intval($params[0]);
        if (empty($category_id))
            $category_id = null;
        $categories = Category::getCategories();
        $format_programs = Format_program::getFormat();
        $format_model = Format_model::getFormat();
        if (Core::getInstance()->requestMethod === 'POST') {
            $_POST['name'] = trim($_POST['name']);
            $errors = [];
            if (empty($_POST['name']))
                $errors['name'] = 'Назва товару не вказана';
            $prod = Product::getProduct();
            if (in_array($_POST['name'], $prod))
                $errors['name'] = 'Назва товару не вказана або товар уже є на сайті';
            if (empty($_POST['category_id']))
                $errors['category_id'] = 'Категорію не вибрано';
            if ($_POST['price'] <= 0)
                $errors['price'] = 'Ціна некоректно задана';
            if (empty($_POST['checkin']))
                $errors['checkin'] = 'Категорію перевірки не задано';
            if (empty($_POST['native_format']))
                $errors['native_format'] = 'Не вибрано рідний формат';
            if (empty($_POST['render_format']))
                $errors['render_format'] = 'Не вибрано рендер формат';
            if (empty($_POST['format_program']))
                $errors['format_program'] = 'Не вибрано формати програм';
            if (empty($_POST['format_model']))
                $errors['format_model'] = 'Не вибрано формати моделі';
            if (empty($_POST['geometry']))
                $errors['geometry'] = 'Не вибрано формат програм';
            if (($_POST['polygons'] <= 0))
                $errors['polygons'] = 'Кількість полігонів некоректно задана';
            if (($_POST['vertices'] <= 0))
                $errors['vertices'] = 'Кількість точок некоректно задана';
            if (empty($_POST['format_texture']))
                $errors['format_texture'] = 'Не задано формат';


            if (empty($errors)) {
                $_POST['user_id'] = $_SESSION['user']['id'];
                Product::addProduct($_POST, $_FILES['file']['tmp_name']);
                return $this->redirect('/product/index');
            } else {
                $model = $_POST;
                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model,
                    'categories' => $categories,
                    'category_id' => $category_id,
                    'format_programs' => $format_programs,
                    'format_model' => $format_model
                ]);
            }
            Product::addProduct($_POST, $_FILES['file']['tmp_name']);
            return $this->redirect('/product');
        }
        return $this->render(null, [
            'categories' => $categories,
            'category_id' => $category_id,
            'format_programs' => $format_programs,
            'format_model' => $format_model
        ]);
    }

    public function editAction($params)
    {
        $id = intval($params[0]);
        if (!User::isAdmin())
            if (!User::isModerator())
                if (!User::isUser())
                    return $this->error(403);
        if ($id > 0) {
            $model = Product::getProductById($id);
            //$OLDname = Product::getProductById($id)['name'];
            if (!empty($model['format_program']))
                $model['format_program'] = Utils::explodeToArray($model['format_program']);
            if (!empty($model['format_model']))
                $model['format_model'] = Utils::explodeToArray($model['format_model']);
            if (!empty($model['format_texture']))
                $model['format_texture'] = Utils::explodeToArray($model['format_texture']);

            $categories = Category::getCategories();
            $format_programs = Format_program::getFormat();
            $format_model = Format_model::getFormat();
            if (Core::getInstance()->requestMethod === 'POST') {
                $_POST['name'] = trim($_POST['name']);
                $errors = [];
                if (empty($_POST['name']))
                    $errors['name'] = 'Назва товару не вказана';
                if (empty($_POST['category_id']))
                    $errors['category_id'] = 'Категорію не вибрано';
                if ($_POST['price'] <= 0)
                    $errors['price'] = 'Ціна некоректно задана';
                if (empty($_POST['checkin']))
                    $errors['checkin'] = 'Категорію перевірки не задано';
                if (empty($_POST['native_format']))
                    $errors['native_format'] = 'Не вибрано рідний формат';
                if (empty($_POST['render_format']))
                    $errors['render_format'] = 'Не вибрано рендер формат';
                if (empty($_POST['format_program']))
                    $errors['format_program'] = 'Не вибрано формати програм';
                if (empty($_POST['format_model']))
                    $errors['format_model'] = 'Не вибрано формати моделі';
                if (empty($_POST['geometry']))
                    $errors['geometry'] = 'Не вибрано формат програм';
                if (($_POST['polygons'] <= 0))
                    $errors['polygons'] = 'Кількість полігонів некоректно задана';
                if (($_POST['vertices'] <= 0))
                    $errors['vertices'] = 'Кількість точок некоректно задана';
                if (empty($_POST['format_texture']))
                    $errors['format_texture'] = 'Не задано формат';

                if (empty($errors)) {

                    //if ($OLDname !== $_POST['name']) {
                    /* $currentpath = 'files/product/' . $_POST['name'];
                    if (is_dir($currentpath)) {

                     } else
                         mkdir($currentpath, 0777, true);*/
                    //rename("files/product/{$OLDname}", "files/product/{$_POST['name']}");
                    /*

                                            if(!empty($_FILES['file']['tmp_name'])) {
                                                if (is_array($_FILES['file']['tmp_name'])) {
                                                    foreach ($_FILES['file']['tmp_name'] as $foto) {
                                                        do {
                                                            $fileName = uniqid() . '.jpg';
                                                            $_POST['photos'] [] = $fileName;
                                                            $newPath = "files/product/{$_POST['name']}/{$fileName}";
                                                        } while (file_exists($newPath));
                                                        move_uploaded_file($foto, $newPath);
                                                    }
                                                } elseif (is_string($_FILES['file']['tmp_name'])) {
                                                    do {
                                                        $fileName = uniqid() . '.jpg';
                                                        $_POST['photos'] [] = $fileName;
                                                        $newPath = "files/product/{$_POST['name']}/{$fileName}";
                                                    } while (file_exists($newPath));
                                                    move_uploaded_file($_FILES['file']['tmp_name'], $newPath);
                                                }
                                            }
                                            Product::recursiveRemoveDir('files/product/'.$OLDname);*/
                    //}
                    /*var_dump(($_FILES['file']['tmp_name'][0]));
                    die;*/
                    Product::updateProduct($id, $_POST);

                    if (!empty($_FILES['file']['tmp_name'][0]))
                        Product::changePhoto($id, $_FILES['file']['tmp_name']);


                    return $this->redirect('/product/index');
                } else {
                    foreach ($_POST as $value)
                        if (!empty($value)) {
                            $model = $_POST;
                            break;
                        }
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'categories' => $categories,
                        'format_programs' => $format_programs,
                        'format_model' => $format_model
                    ]);
                }
            }
            return $this->render(null, [
                'model' => $model,
                'categories' => $categories,
                'format_programs' => $format_programs,
                'format_model' => $format_model
            ]);
        } else
            return $this->error(403);
    }

    public function deleteAction($params)
    {
        $id = intval($params[0]);
        $yes = boolval($params[1] === 'yes');
        if (!User::isAdmin())
            if (!User::isModerator())
                if (!User::isUser())
                    return $this->error(403);
        if ($id > 0) {
            $product = Product::getProductById($id);
            if ($yes) {
                $product['photos'] = Utils::explodeToArray($product['photos']);
                if (is_array($product['photos']))
                    foreach ($product['photos'] as $foto) {
                        $filePath = 'files/product/' . $product['name'] . '/' . $foto;
                        if (is_file($filePath))
                            unlink($filePath);
                    }

                Product::recursiveRemoveDir('files/product/' . Product::getProductById($id)['name']);
                //rmdir(Product::getProductById($id)['name']);
                $prod = Product::getProductById($id);
                Statistic::deleteStatistic($prod["statistics_id"]);
                Product::deleteProduct($id);


                return $this->redirect('/product/index');
            }
            return $this->render(null, [
                'product' => $product
            ]);
        } else
            return $this->error(403);
    }

    public function viewAction($params)
    {
        $id = intval($params[0]);
        $product = Product::getProductById($id);
        $statistic = Statistic::getStatisticById($product["statistics_id"]);
        if (Core::getInstance()->requestMethod != 'POST') {
            if ($product["user_id"] != $_SESSION["user"]["id"]) {
                $statistic["number_of_views"]++;
                Statistic::updateStatistic($statistic['id'], $statistic);
            }
        }
        $format_prog = Format_program::getFormatById($product['native_format']);
        if (!empty($product['format_program']))
            $product['format_program'] = Utils::explodeToArray($product['format_program']);
        if (!empty($product['format_model']))
            $product['format_model'] = Utils::explodeToArray($product['format_model']);
        if (!empty($product['format_texture']))
            $product['format_texture'] = Utils::explodeToArray($product['format_texture']);
        if (!empty($product['photos']))
            $product['photos'] = Utils::explodeToArray($product['photos']);


        if (Core::getInstance()->requestMethod === 'POST') {
            $errors = [];
            if ($_POST['product_id'] != $id)
                $errors['product_id'] = 'Невірний ID продукту!';
            if (empty($errors)) {
                Basket::addProduct($_POST['product_id']);
                //return $this->redirect('/product/index');
            } else {
                return $this->render(null, [
                    'errors' => $errors,
                    'product' => $product
                ]);
            }
            //return $this->redirect('/product');
        }


        return $this->render(null, [
            'product' => $product,
            'statistics_id' => $statistic,
            'format_prog' => $format_prog
        ]);
    }
}