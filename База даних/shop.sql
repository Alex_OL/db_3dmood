-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Чрв 09 2023 р., 11:33
-- Версія сервера: 5.6.51
-- Версія PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `shop`
--

-- --------------------------------------------------------

--
-- Структура таблиці `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Назва категорії',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `category`
--

INSERT INTO `category` (`id`, `name`, `photo`) VALUES
(1, 'Автомобілі', '63a9b02749f32.jpg'),
(2, 'Персонажі', '63c594c3e4d11.jpg'),
(3, 'Транспортні засоби', '63a9b24a3c6d2.jpg'),
(4, 'Архітектура', '63a9b2077697a.jpg'),
(5, 'Меблі', '63c595dad1736.jpg'),
(6, 'Анатомія', '63a9b22445556.jpg'),
(7, 'Пейзажі', '63a9b22dd2aa4.jpg'),
(8, 'Технології', '63c59616531f3.jpg'),
(9, 'Тварини', '63a9b25fedb21.jpg');

-- --------------------------------------------------------

--
-- Структура таблиці `format_model`
--

CREATE TABLE `format_model` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `format_model`
--

INSERT INTO `format_model` (`id`, `name`) VALUES
(1, '3D Studio'),
(2, 'OBJ'),
(3, 'FBX'),
(4, 'DXF'),
(5, 'FBX Metallic v'),
(6, 'FBX Specular v'),
(7, 'glTF 2.0');

-- --------------------------------------------------------

--
-- Структура таблиці `format_program`
--

CREATE TABLE `format_program` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `format_program`
--

INSERT INTO `format_program` (`id`, `name`) VALUES
(1, '3ds Max'),
(2, 'Maya'),
(3, 'Cinema 4D'),
(4, 'Unity 5.4'),
(5, 'Unreal 4.10'),
(6, 'test');

-- --------------------------------------------------------

--
-- Структура таблиці `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Назва товару',
  `category_id` int(11) DEFAULT NULL COMMENT 'Категорія товару',
  `price` double NOT NULL COMMENT 'Ціна',
  `description` text COLLATE utf8mb4_unicode_ci COMMENT 'Опис',
  `visible` int(11) NOT NULL DEFAULT '1' COMMENT 'Видимість моделі',
  `checkin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Перевірка моделі',
  `format_program` text COLLATE utf8mb4_unicode_ci COMMENT 'Формат програм які підтримує модель',
  `format_model` text COLLATE utf8mb4_unicode_ci COMMENT 'Формати моделі',
  `polygons` int(11) NOT NULL DEFAULT '1' COMMENT 'Кількість полігонів',
  `vertices` int(11) NOT NULL DEFAULT '1' COMMENT 'Кількість точок',
  `format_texture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Текстури',
  `native_format` int(11) DEFAULT NULL COMMENT 'Рідний формат',
  `render_format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Рендер формат',
  `geometry` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Тип геометрії',
  `photos` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL COMMENT 'ID Користувача',
  `statistics_id` int(11) DEFAULT NULL COMMENT 'Статистика продукту'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `price`, `description`, `visible`, `checkin`, `format_program`, `format_model`, `polygons`, `vertices`, `format_texture`, `native_format`, `render_format`, `geometry`, `photos`, `user_id`, `statistics_id`) VALUES
(1, 'Tesla Model 3', 1, 99, '<p><strong>Tesla Model 3</strong> is a high quality, photo real model that will enhance detail and realism to any of your rendering projects. With detailed interior. Doors can be opened. The model has a fully textured, detailed design that allows for close-up renders, and was originally modeled in 3ds Max 2012 and rendered with V-Ray. Renders have no postprocessing.<br>Hope you like it!<br><br><i>Tesla Model 3 is ready to be animated. You can easily position it the way you need or animate.</i><br>*********************************<br><i>Features:</i><br>- High quality polygonal model, correctly scaled for an accurate representation of the original object.<br>- Models resolutions are optimized for polygon efficiency. (In 3ds Max, the Meshsmooth function can be used to increase mesh resolution if necessary.)<br>- All colors can be easily modified.<br>- Model is fully textured with all materials applied.<br>- All textures and materials are included and mapped in every format.<br>- 3ds Max models are grouped for easy selection, and objects are logically named for ease of scene management.<br>- No part-name confusion when importing several models into a scene.<br>- No cleaning up necessaryjust drop your models into the scene and start rendering.<br>- No special plugin needed to open scene.<br>- Model does not include any backgrounds or scenes used in preview images.<br>- Units: cm<br>- Photoshop (original texture files with all layers)<br>*********************************<br><strong>File Formats:</strong><br>- 3ds Max 2012 V-Ray and standard materials scenes<br>- OBJ (Multi Format)<br>- 3DS (Multi Format)<br>- Maya 2011<br>- Cinema 4D R14<br>- FBX (Multi Format)<br>- Photoshop (original texture files with all layers)<br>*********************************<br>Textures Formats:<br>- (8 .png) 4096 x 4096<br>- (1 .png) 2048 x 2048<br><br><strong>Also there are original texture files with all layers .PSD format in particular archive</strong><br>*********************************<br>3d Molier International is a team of 3D artists with over a decade of experience in the field. The company participated in various projects allowing us to learn our clients needs. Every model we build goes through thorough Quality assessment both visual and technical to make sure the assets look realistic and the models are of the best quality, which you can tell by looking at the renders none of the has any postprocessing. On the top of that all the models come with complete UVs and optimized topology, which allows you in no time alter geo or the textures if needed.<br><br>Also check out our other models, just click on our user name to see complete gallery.<br>3d_molier International 2017</p>', 1, 'CheckMate Pro', '3ds Max ~/!\\~ Maya ~/!\\~ Cinema 4D ~/!\\~ Unity 5.4 ~/!\\~ Unreal 4.10', '3D Studio ~/!\\~ OBJ ~/!\\~ FBX', 116225, 130476, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 1, 'Corona', 'Polygonal Quads/Tris Geometry', '63c59c1581ab1.jpg ~/!\\~ 63c59c15820a5.jpg ~/!\\~ 63c59c1582647.jpg ~/!\\~ 63c59c1582afe.jpg ~/!\\~ 63c59c1582f47.jpg', 1, 1),
(2, 'Mercedes-Benz EQS', 1, 199, '<p>Photorealistic, high-quality and accurate 3D model of 2022 Mercedes-Benz EQS with high detailed interior.<br>The model is suitable for Ultra HD rendering and further use in broadcast, high-res film close-up, advertising, etc.<br>The model can be also used in real-time applications (Unreal Engine, Unity and etc.) after a number of mesh optimizations, texture backing and other procedures executed on the customer\'s side.<br>The studio rendering quality shown on product previews is reachable \'out-of-box\' with no need of any additional manipulations, if you use 3ds max + Corona native file format.<br>Just download, open the file and hit \'Render\'!<br><br><strong>Main features:</strong><br><i>- Originally created with 3ds Max 2015</i><br>- Two MAX files with Corona and Standard 3ds Max materials<br>- Subdivision modifier is available in the history for MAX, MB and C4D file formats. You can get access to original non-smoothed mesh by deactivating the Subdiv modifier<br>- Separate objects like body, wheels and doors. Total number of objects is 27<br>- Doors, trunk and charging cap can be opened<br>- Unit system is set to metric (millimeters). Real-world scale<br>- All textures and materials are included and assigned<br>- 28 textures, which vary in resolution from 33x796px to 8000x8000px<br>- Windshield, rear window and back side windows are using high resolution transparency mask texture<br>- Previews rendered in 3ds Max 2015 with Corona 5.0. Scenes, HDRI\'s and backplate images for outdoor renders are not included in product files<br><br><strong>Special notes:</strong><br>- FBX and OBJ formats are recommended for import in other 3d software. If your software doesn\'t support FBX or OBJ format, please use 3DS format; FBX and OBJ formats were exported from 3ds Max. The geometry for FBX and OBJ formats is set to quads<br>- FBX format is recommended for importing in real-time engines like Unreal Engine, Unity and etc.<br>- MAX files can be loaded in Autodesk 3ds Max 2015 or higher. In order to use Corona rendering setups and materials, Corona 3.0 or higher is needed<br>- C4D file can be loaded in MAXON Cinema4D R17 or higher. Only standard Cinema4D materials and standard renderer used<br>- MB file can be loaded in Autodesk Maya 2015 or higher. Only standard Maya materials and standard Hardware 2.0 renderer used<br><br><strong>Polycounts:</strong><br>MAX format (one file included with TurboSmooth iteration 1 applied to all objects):<br>&nbsp;&nbsp;&nbsp;no TurboSmooth: 473,055 faces and 515,384 vertices<br>&nbsp;&nbsp;&nbsp;TurboSmooth iteration 1: 1,888,230 faces and 1,975,430 vertices<br>&nbsp;&nbsp;&nbsp;TurboSmooth iteration 2: 7,552,920 faces and 7,725,872 vertices<br><br><strong>MB and C4D formats (only one file for each format included with Subdivision iteration 1 applied to all objects):</strong><br>&nbsp;&nbsp;&nbsp;no Subdiv: 473,055 faces and 515,384 vertices<br>&nbsp;&nbsp;&nbsp;Subdiv iteration 1: 1,888,230 faces and 1,975,430 vertices<br>&nbsp;&nbsp;&nbsp;Subdiv iteration 2: 7,552,920 faces and 7,725,872 vertices<br><br><strong>FBX and OBJ formats (3 files included for each format):</strong><br>&nbsp;&nbsp;&nbsp;low: 473,055 faces and 515,384 vertices<br>&nbsp;&nbsp;&nbsp;medium: 1,888,230 faces and 1,975,430 vertices<br>&nbsp;&nbsp;&nbsp;high: 7,552,920 faces and 7,725,872 vertices<br><br><strong>3DS format (3 files included):</strong><br>&nbsp;&nbsp;&nbsp;low: 942,120 faces and 574,098 vertices<br>&nbsp;&nbsp;&nbsp;medium: 3,776,460 faces and 2,117,213 vertices<br>&nbsp;&nbsp;&nbsp;high: 15,105,840 faces and 8,210,643 vertices</p>', 1, 'CheckMate Pro', '3ds Max ~/!\\~ Maya ~/!\\~ Cinema 4D', '3D Studio ~/!\\~ OBJ ~/!\\~ FBX', 473055, 515384, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', NULL, 'Corona', 'Polygonal Quads/Tris Geometry', '63c59cfc6875d.jpg ~/!\\~ 63c59cfc68f95.jpg ~/!\\~ 63c59cfc695d7.jpg ~/!\\~ 63c59cfc69c43.jpg ~/!\\~ 63c59cfc6a155.jpg ~/!\\~ 63c59cfc6a59b.jpg ~/!\\~ 63c59cfc6a8ff.jpg', 1, 2),
(3, 'Wolf RIG XGEN', 9, 999, '<p>High Detailed Photorealistic Wolf; UVunwrapped and smoothable, made with Maya. Xgen for hair (no plug-ins needed)<br>This model has a clean geometry based on loops and only quads. It is FULLY RIGGED and comes with&nbsp;</p><p>Maya settings for Arnold renderer.<br>(Ready to render exactly as you see above)<br>eyes, corneas, gum/teeth and tongue are modeled separately.<br>The model is provided in the default RIG pose and signature pose as well (rigged).<br><br><strong>BlendShapes included:</strong><br><strong>Blink</strong><br><strong>Grin</strong><br><br>-19 High resolution textures:<br>-the main color map is 8k<br>-the displacement map is 8k (.exr 32bit)<br><br>Measurement units: centimeters<br>-<br><strong>Formats included:</strong><br>-Maya 2020-<br>GreyWolfRigMaya2020.ma default rigging pose (maya 2020 ascii)<br>GreyWolf_SigPoseMaya2020.ma - signature pose (maya 2020 ascii)<br>-<br>Mesh resolution Polycount (quads):<br>main wolf mesh: 6014<br>eyeballs/corneas: 224<br>gum-teeth: 2778<br>tongue: 508<br>whiskers: 6500<br>background plane: 1<br>***********************<br><strong>RIG NOTE:</strong><br>The rig is completely flexible and has smart Ctrls, for good deformations. Extra jiggles have&nbsp;</p><p>been added in key areas for an added skin deformation control.<br>Below are some features of the rig:<br>Right Click Menu<br>Mirror pose for selected Ctrl<br>Space Switching<br>IKFK Controller<br>Auto Clavicle<br>Global Scale<br>Stretch Arm/Leg<br>Elbow/Knee Pin<br>Bendy Arm/Leg<br>FootRoll with options IK+FK Spine with smart Controller<br>Squeeze+Squash Spine<br>Colored Ctrls.<br>And more..<br>BlendShapes included (connected to ML_Head_Skull_CtrlShape):<br>Blink<br>Grin<br>***********************<br><strong>Xgen Notes:</strong><br>In order to avoid Maya crashing when opening the file the fur splines in the main fur descriptions have been reduced to 10%.<br>mainLong main_base 1.500 (actual value) you need to switch back to 15.000 to get the preview outputs<br>main main_base 1.000 (actual value) you need to switch back to 10.000 to get the preview outputs<br>***********************<br><strong>OTHER NOTES:</strong><br>-The model is provided in the standard RIG pose and signature pose as well.</p>', 1, 'CheckMate Pro', 'Maya', '3D Studio ~/!\\~ FBX', 16024, 16374, 'Rigged ~/!\\~ Animated', 1, 'Other', 'Polygonal Quads/Tris Geometry', '63c66346f3a60.jpg ~/!\\~ 63c66346f3bf1.jpg ~/!\\~ 63c66346f3cfa.jpg ~/!\\~ 63c66346f3e09.jpg ~/!\\~ 63c66346f3f09.jpg', 9, 3),
(4, 'Lion', 9, 1099, '<p>High Detailed Photorealistic Lion; FULLY RIGGED and ANIMATED, UVunwrapped and smoothable, made with Maya. Shave and a haircut for hair.<br>This model has a clean geometry based on loops and only quads. It is FULLY RIGGED and ANIMATED and comes with Maya settings for Arnold renderer.<br>(Ready to render exactly as you see above)<br>eyes, corneas, gum, teeth, tongue and claws are modeled separately.<br><br>12 High resolution textures:<br>-color.bmp (81928192) -bump.jpg (81928192) -specular.jpg (81928192) -displacement.exr(32bit) (81928192)<br>-eyeColor.bmp (20482048) -eyeSpecular.jpg (20482048) -eyeBump.jpg (20482048) -furHeadClumps.jpg (20482048) furManeClumps.jpg (20482048)-furDensity.jpg (81928192)<br>-furBodyClumps.jpg (20482048) -furDynamics.jpg (20482048) -ibl.jpg bkg.jpg<br><br>Measurement units: centimeters<br><br><strong>Formats included:</strong><br>-Maya 2018-<br>LionRIG_Maya2018_Shave96v16.ma6.ma default -rigging pose (maya 2018 ascii)<br>LionSignaturePose.ma - Signature pose<br>walkAnim.ma- walk cycle + grin referenced to the main rig (maya 2018 ascii)<br>hairCurvesExportedMaya2018.ma hair curves (nurbs) converted from shave and a haircut hair primitives (maya 2018 ascii)<br><br>-Fbx-<br>hairCurvesExported.fbx hair curves (nurbs) converted from shave and a haircut hair primitives.<br><br><strong>Mesh resolutions:</strong><br><br>Polycount (quads):<br>lion main: 30450<br>eyeballs/corneas: 288<br>gum-teeth: 6144<br>tongue: 572<br>claws: 3420<br>background plane: 1<br>***********************<br>SHAVE AND A HAIRCUT NOTE:<br>You would need Shave and A Haircut for Maya 2018 (*/**) in order to render the hair, otherwise</p><p>&nbsp;you would be able to render only the main mesh.<br>For an even better render quality you can increase the hair primitives count up to your machine memory&nbsp;</p><p>limit (The actual renders have been made on a machine based on a single I7 cpu with 16 gigs of ram about 6 minutes for an HD frame)<br>11 different hair systems for a total of about 3 millions hairs.<br>Included: Exported hair curves created by hair primitives (FBX and Maya 2018 ascii) so you can use them as guides if you re using a different Fur/Hair plugin.<br>* The actual files have been created with Shave and a haircut 9.6<br>** Please always use an updated Shave and a haircut release to avoid rendering issues.<br>***********************<br><strong>RIG NOTE:</strong><br><br>The rig is completely flexible and has smart Ctrls, for good deformations.<br>The blendShapes are connected to the highpoly mesh (ML_Head_Upper_Jaw_Ctrl): eye blink/roar<br>Below are some features of the rig.<br>-Right Click Menu<br>-Mirror pose for selected Ctrl<br>-Space Switching<br>-IKFK Controller<br>-Auto Clavicle<br>-Global Scale<br>-Stretch Arm/Leg<br>-Elbow/Knee Pin<br>-Bendy Arm/Leg<br>-FootRoll with options IK+FK Spine with smart Controller<br>-Squeeze+Squash Spine<br>-Indvidual fingers Ctrls.<br>And more..<br><br><strong>ANIMATION NOTE:</strong><br>walk cycle + grin included 71 frames Ready to render exactly as you see in the preview.</p><p>&nbsp;The dynamics are setup, you just need to set your STAT directory and run dynamics from Shave menu.<br><br><strong>***********************</strong><br><strong>ARNOLD RENDERER NOTE:</strong><br>* The actual rendering settings (AA=5) are for a medium quality output (preview images shown above).&nbsp;</p><p>To get better quality images you can increase the Arnold settings up to your machine limit.<br>&nbsp;</p>', 1, 'CheckMate Pro', 'Maya', '3D Studio ~/!\\~ FBX ~/!\\~ FBX Metallic v ~/!\\~ FBX Specular v', 40874, 41129, 'Textures ~/!\\~ Materials ~/!\\~ Rigged ~/!\\~ Animated ~/!\\~ UV Mapped', 1, 'Maxwell', 'Polygonal Quads/Tris Geometry', '63c6644cb1dfe.jpg ~/!\\~ 63c6644cb1fa1.jpg ~/!\\~ 63c6644cb20b2.jpg ~/!\\~ 63c6644cb2291.jpg', 9, 4),
(5, 'Container Ship', 3, 175, '<p>- A custom model similar to the largest container ships in the world (21 containers in length by 20 containers in width, on one level);<br>- The ship and containers are made out of 139364 polygons (only quads and tris);<br>- The model is fully unwrapped (no overlapping, with the exception of the containers) see last presentation image;<br>- The ship model has three 14000 pixels maps: diffuse, reflection, and refraction, and i also included a HDRI image, for the environment;<br>- There are two types of diffuse maps: original - as shown in the first presentation images, and clean (without scratches smoke marks and dirt) - as shown in the last three presentation images, so you can use either one of them;<br>- All brands and logos on the ship and on the containers are fictive;<br>- The model is aligned with the grid, and you don\'t need any special pluggin in order to open the scene.</p>', 1, 'Без перевірки', '3ds Max ~/!\\~ Unity 5.4 ~/!\\~ Unreal 4.10', 'OBJ ~/!\\~ FBX', 139364, 152183, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 3, 'V-Ray', 'Polygonal Quads/Tris Geometry', '63c665445540b.jpg ~/!\\~ 63c66544555ea.jpg ~/!\\~ 63c6654455734.jpg ~/!\\~ 63c665445596b.jpg', 9, 5),
(6, 'Formula 1 Season 2022', 1, 99, '<p>Low-poly subdivision-ready 3D model of Formula 1 Season 2022 F1</p><p>&nbsp;Race Car Concept with PBR materials (Specular and Metallic workflows).<br><br><strong>Advantages of the model:</strong><br>- Very clean geometry;<br>- The model is maximally optimized for use in 3d games and VR;<br>- 4 Materials with textures set - separate for Body, Carbon, Common, Wheels - containing AO,&nbsp;</p><p>BaseColor/Diffuse, Emissive, Glossiness/Roughness, Metallic/Specular, Normal - depending on material;<br>- Textures size:<br>Body - 4,096 X 4,096 PNG;<br>Carbon - 4,096 X 4,096 PNG;<br>Common - 4,096 X 4,096 PNG;<br>Wheels - 4,096 X 4,096 PNG;<br>-Very low polycount. The model without turbosmooth is great for mobile games. When you apply a turbosmooth modifier,&nbsp;</p><p>it starts to look great in large projects on PCs and game consoles as well. This is possible thanks to a very competent application of smoothing groups and properly adjusted geometry;<br>- Real world scale (Metric / Centimeters). So you can import this model easily into your project without need to change anything;<br>- The model uses smoothing groups, so you can always increase details to the required level simply by applying a turbosmooth&nbsp;</p><p>modifier with a necessary number of iterations;<br>- Divided into functional parts and grouped for easy editing and using in game engines;<br>- The model consists of 38 separate objects grouped together. Each of these objects has a simple and understandable name;<br>-UV map is maximally optimized for texture\'s easy editing so without difficulties you can add or remove any logo or some details where needed;<br>- Specular and Metallic workflow textures included!<br>- 3D model archieves include separate layers of diffuse texture thus making changes to textures is easy as never.<br>- Compatible with the majority of modern game engines!<br><br><strong>Model specifications:</strong><br><br>Created in 3DS Max 2018 and rendered with V-Ray 4.10. Lighting is a mix of global illumination from HDRI (included) and 1 v-ray light.&nbsp;</p><p>The model does not require installation of additional plug-ins or software. You need only 3DS Max 2018 and V-Ray 4.10.03 to get the same rendering as on the preview image.<br><br>- Polygons count: 27,112 (no n-gons)<br>Subdivision 1 polygons count: 215,370<br>Subdivision 2 polygons count: 861,480<br>- Vertices count: 27,561<br>- Unit Scale: Metric / Centimeters<br><br><i><strong>Materials:</strong></i><br>A V-Ray PBR material with specular workflow is applied to the model. All its parameters were created for the StemCell standard.<br><br><strong>Model is available in the following formats:</strong><br>- MAX (2018)<br>- FBX<br>- OBJ<br>- 3DS<br>- DXF (2010)<br>- STL<br>&nbsp;</p>', 1, 'StemCell', '3ds Max ~/!\\~ Maya ~/!\\~ Cinema 4D ~/!\\~ Unity 5.4 ~/!\\~ Unreal 4.10', '3D Studio ~/!\\~ OBJ ~/!\\~ FBX ~/!\\~ DXF ~/!\\~ FBX Metallic v ~/!\\~ FBX Specular v ~/!\\~ glTF 2.0', 27112, 27561, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 3, 'V-Ray', 'Polygonal Quads/Tris Geometry', '63c66ae70b101.jpg ~/!\\~ 63c66ae70b4c1.jpg ~/!\\~ 63c66ae70b69a.jpg ~/!\\~ 63c66ae70b9ec.jpg ~/!\\~ 63c66ae70bbc0.jpg ~/!\\~ 63c66ae70bcf1.jpg ~/!\\~ 63c66ae70be2e.jpg', 10, 6),
(7, 'The City of the Future', 4, 1177, '<p>Detailed coastal area, with office buildings, exhibition complexes, business and shopping centres, residential buildings and various infrastructure.<br><br><br><strong>The size of the entire area ~ 2 x 1.1 kilometers</strong><br><br>Scene Totals:<br>Objects: 1252.<br>Sun: 2 positions.<br>Cameras: 21.<br>Shapes: 62.<br><br>Texture: 28<br><br><strong>Scene previews were rendered with V-Ray for 3ds Max 2016.</strong><br><br><i><strong>Mesh Totals:</strong></i><br>Faces: 1925768<br>Verts: 2690663</p>', 1, 'Без перевірки', '3ds Max ~/!\\~ Unity 5.4 ~/!\\~ Unreal 4.10', 'OBJ ~/!\\~ FBX ~/!\\~ FBX Metallic v ~/!\\~ FBX Specular v', 1925768, 2690663, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 3, 'V-Ray', 'Polygonal Quads only', '63c66d12a6c5b.jpg ~/!\\~ 63c66d12a6e49.jpg ~/!\\~ 63c66d12a6fb4.jpg ~/!\\~ 63c66d12a7220.jpg ~/!\\~ 63c66d12a73b4.jpg ~/!\\~ 63c66d12a751a.jpg', 10, 7),
(8, 'Robot', 2, 299, '<p>This&nbsp;is&nbsp;Qualitative&nbsp;3d&nbsp;model&nbsp;Robot&nbsp;that&nbsp;can&nbsp;be&nbsp;used&nbsp;in&nbsp;Animation/Films/Games.&nbsp;&nbsp;<br>Package&nbsp;contains&nbsp;Character&nbsp;in&nbsp;Midpoly&nbsp;and&nbsp;Lowpoly&nbsp;Resolution&nbsp;high-res&nbsp;textured&nbsp;for&nbsp;PBR&nbsp;rendering.&nbsp;&nbsp;<br>UNITY&nbsp;5&nbsp;Package&nbsp;and&nbsp;Unreal&nbsp;Migrate&nbsp;included!&nbsp;&nbsp;<br>-&nbsp;Still&nbsp;pictures&nbsp;showing&nbsp;the&nbsp;midpoly&nbsp;version.&nbsp;<br>-&nbsp;Turntables&nbsp;showing&nbsp;the&nbsp;low&nbsp;poly&nbsp;version.&nbsp;<br>-&nbsp;Preview&nbsp;video&nbsp;shows&nbsp;character&nbsp;in&nbsp;Unreal&nbsp;Engine&nbsp;<br>&nbsp;<br><strong>--------------LOWPOLY&nbsp;Version&nbsp;(Description)--------</strong><br>&nbsp;This&nbsp;Resolution&nbsp;is&nbsp;preferably&nbsp;for&nbsp;using&nbsp;In-game&nbsp;&nbsp;&nbsp;&nbsp;<br>-&nbsp;67.135&nbsp;Polys&nbsp;107.995&nbsp;Tris&nbsp;58.188&nbsp;Verts&nbsp;&nbsp;<br>-&nbsp;Rigged&nbsp;and&nbsp;exported&nbsp;to&nbsp;Unity&nbsp;and&nbsp;Unreal<br><br><strong>--------------MIDPOLY&nbsp;Version&nbsp;(Description)--------&nbsp;&nbsp;</strong><br>This&nbsp;Resolution&nbsp;is&nbsp;preferably&nbsp;to&nbsp;use&nbsp;for&nbsp;keyart&nbsp;and&nbsp;rendering&nbsp;<br>-&nbsp;Textures&nbsp;partly&nbsp;nodebased&nbsp;partly&nbsp;bitmap&nbsp;textures&nbsp;<br>-&nbsp;Without&nbsp;Subdivision&nbsp;Modifiers:&nbsp;&nbsp;362.876&nbsp;Polys&nbsp;634.290&nbsp;Tris&nbsp;320.935&nbsp;Verts<br>&nbsp;-&nbsp;not&nbsp;rigged&nbsp;in&nbsp;A-pose&nbsp;&nbsp;<br><strong>----------------------------Unity5--------------------------------------&nbsp;&nbsp;</strong><br>Robot&nbsp;Unity&nbsp;Project&nbsp;contains:&nbsp;<br>-&nbsp;The&nbsp;scene&nbsp;with&nbsp;HDR&nbsp;setup&nbsp;<br>-&nbsp;\'Low&nbsp;Poly\'&nbsp;Version&nbsp;Character&nbsp;&nbsp;<br>-&nbsp;Calibrated&nbsp;Textures&nbsp;for&nbsp;Unity&nbsp;PBR&nbsp;Standard&nbsp;material&nbsp;&nbsp;<br>-&nbsp;Metallness-Smoothness&nbsp;Workflow&nbsp;<br>-&nbsp;Material&nbsp;and&nbsp;Textures&nbsp;applied&nbsp;<br>-&nbsp;Rig&nbsp;is&nbsp;prepared&nbsp;for&nbsp;Unity&nbsp;to&nbsp;work&nbsp;with&nbsp;Humanoid&nbsp;and&nbsp;Generic&nbsp;Animation&nbsp;Systems&nbsp;&nbsp;<br><strong>----------------------------Unreal&nbsp;---------------------------------&nbsp;&nbsp;</strong><br>-&nbsp;Migrate&nbsp;files&nbsp;to&nbsp;easy&nbsp;import&nbsp;into&nbsp;Unreal&nbsp;Engine&nbsp;<br>-&nbsp;Mannequin&nbsp;skeleton&nbsp;and&nbsp;Mannequin&nbsp;size&nbsp;<br>-&nbsp;Character&nbsp;can&nbsp;use&nbsp;mannequin&nbsp;animations&nbsp;<br>-&nbsp;Unreal&nbsp;Cloth&nbsp;setup&nbsp;(&nbsp;Cloth&nbsp;behaves&nbsp;naturally&nbsp;with&nbsp;animations)&nbsp;<br>-&nbsp;textures&nbsp;calibrated&nbsp;and&nbsp;included&nbsp;in&nbsp;migrate&nbsp;files&nbsp;<br>-&nbsp;swords&nbsp;are&nbsp;exported&nbsp;separately&nbsp;and&nbsp;can&nbsp;be&nbsp;added&nbsp;or&nbsp;removed&nbsp;&nbsp;<br>Please&nbsp;see&nbsp;preview&nbsp;video&nbsp;that&nbsp;shows&nbsp;character&nbsp;with&nbsp;basic&nbsp;mannequin&nbsp;animations&nbsp;in&nbsp;unreal&nbsp;test&nbsp;scene.&nbsp;&nbsp;<br><strong>&nbsp;------------------------------------FBX---------------------&nbsp;</strong><br>&nbsp;Unity&nbsp;5&nbsp;FBX&nbsp;-&nbsp;Exported&nbsp;Low&nbsp;Poly&nbsp;Character&nbsp;for&nbsp;Unity&nbsp;Humanoid&nbsp;Rig&nbsp;<br><strong>&nbsp;---------------------------Blender--------------------------&nbsp;</strong><br>Blender&nbsp;File&nbsp;with&nbsp;Mid&nbsp;and&nbsp;Low&nbsp;Poly&nbsp;resolution&nbsp;Character.</p>', 1, 'Без перевірки', 'Cinema 4D', '3D Studio ~/!\\~ FBX', 67042, 58100, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 3, 'V-Ray', 'Polygonal Quads/Tris Geometry', '63c5a1e0b3f4d.jpg ~/!\\~ 63c5a1e0b4556.jpg ~/!\\~ 63c5a1e0b4a6f.jpg ~/!\\~ 63c5a1e0b5174.jpg', 1, 8),
(47, 'Bentley Continental GT3', 1, 89, '<p>This&nbsp;is&nbsp;low-polygonal&nbsp;subdivision-ready&nbsp;3d&nbsp;model&nbsp;of&nbsp;the&nbsp;Bentley Continental GT3 Pikes Peak Hill Climb racing&nbsp;with&nbsp;PBR&nbsp;materials&nbsp;(Specular&nbsp;and&nbsp;Metallic&nbsp;workflows).<br><strong>The&nbsp;project&nbsp;\'3DS&nbsp;Max\'&nbsp;contains:</strong><br>-&nbsp;The&nbsp;model&nbsp;Bentley Continental GT3 Pikes Peak&nbsp;of&nbsp;47&nbsp;separate&nbsp;objects&nbsp;grouped&nbsp;together.<br>-&nbsp;Each&nbsp;individual&nbsp;object&nbsp;has&nbsp;an&nbsp;understandable&nbsp;name.<br>-&nbsp;Total&nbsp;polygons&nbsp;in&nbsp;scene.&nbsp;N-Gons&nbsp;and&nbsp;Triangle&nbsp;faces&nbsp;20438.<br>-&nbsp;Total&nbsp;Scene&nbsp;Vertices&nbsp;Count&nbsp;21798.<br>-&nbsp;Specular&nbsp;and&nbsp;Metallic&nbsp;workflow&nbsp;textures&nbsp;included.&nbsp;Folder&nbsp;of&nbsp;Textures.<br>-&nbsp;Unit&nbsp;Scale:&nbsp;Metric&nbsp;/&nbsp;Centimeters.<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Model&nbsp;optimized&nbsp;for&nbsp;use&nbsp;in&nbsp;games.&nbsp;Just&nbsp;three&nbsp;materials&nbsp;and&nbsp;three&nbsp;sets&nbsp;of&nbsp;textures&nbsp;for&nbsp;the&nbsp;whole&nbsp;model.</p><p>&nbsp;Increase&nbsp;the&nbsp;detail&nbsp;to&nbsp;the&nbsp;required&nbsp;level&nbsp;simply&nbsp;by&nbsp;applying&nbsp;a&nbsp;turbosmooth&nbsp;modifier.<br>&nbsp;&nbsp;&nbsp;&nbsp;You&nbsp;can&nbsp;easily&nbsp;import&nbsp;this&nbsp;model&nbsp;in&nbsp;your&nbsp;project&nbsp;without&nbsp;the&nbsp;need&nbsp;to&nbsp;change&nbsp;anything.<br>&nbsp;&nbsp;&nbsp;&nbsp;Previews&nbsp;rendered&nbsp;in&nbsp;3ds&nbsp;Max&nbsp;2016&nbsp;with&nbsp;V-Ray.<br>The&nbsp;model&nbsp;does&nbsp;not&nbsp;require&nbsp;the&nbsp;installation&nbsp;of&nbsp;any&nbsp;additional&nbsp;plug-ins&nbsp;or&nbsp;software.<br>-&nbsp;Textures&nbsp;size:<br>Body&nbsp;-&nbsp;4,096&nbsp;X&nbsp;4,096&nbsp;PNG;<br>Glasses&nbsp;-&nbsp;4,096&nbsp;X&nbsp;4,096&nbsp;PNG;<br>Wheels&nbsp;-&nbsp;4,096&nbsp;X&nbsp;4,096&nbsp;PNG;<br><br><strong>Textures&nbsp;(Specular&nbsp;workflow):</strong><br><i>-&nbsp;&nbsp;&nbsp;Diffuse</i><br><i>-&nbsp;&nbsp;&nbsp;Specular</i><br><i>-&nbsp;&nbsp;&nbsp;Glossiness</i><br><i>-&nbsp;&nbsp;&nbsp;NormalMap</i><br><i>-&nbsp;&nbsp;&nbsp;Refraction</i><br><i>-&nbsp;&nbsp;&nbsp;Emissive</i><br><i>-&nbsp;&nbsp;&nbsp;Ambient&nbsp;Occlusion</i><br><br><br><strong>Textures&nbsp;(Metallic&nbsp;workflow):</strong><br><i>-&nbsp;&nbsp;&nbsp;Base&nbsp;Color</i><br><i>-&nbsp;&nbsp;&nbsp;Metallic</i><br><i>-&nbsp;&nbsp;&nbsp;NormalMap</i><br><i>-&nbsp;&nbsp;&nbsp;Roughness</i><br><i>-&nbsp;&nbsp;&nbsp;Refraction</i><br><i>-&nbsp;&nbsp;&nbsp;Emissive</i><br><i>-&nbsp;&nbsp;&nbsp;Ambient&nbsp;Occlusion</i><br><br><strong>Click&nbsp;my&nbsp;nickname&nbsp;for&nbsp;more&nbsp;models!</strong></p>', 1, 'StemCell', '3ds Max ~/!\\~ Maya ~/!\\~ Cinema 4D ~/!\\~ Unity 5.4 ~/!\\~ Unreal 4.10', '3D Studio ~/!\\~ OBJ ~/!\\~ FBX ~/!\\~ DXF ~/!\\~ FBX Metallic v ~/!\\~ FBX Specular v ~/!\\~ glTF 2.0', 20438, 21798, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 3, 'V-Ray', 'Polygonal Quads/Tris Geometry', '63c67084e1b1c.jpg ~/!\\~ 63c67084e1d15.jpg ~/!\\~ 63c67084e1e3e.jpg ~/!\\~ 63c67084e1f4d.jpg ~/!\\~ 63c67084e2056.jpg ~/!\\~ 63c67084e215c.jpg', 1, 9),
(48, 'Old Classic Library', 5, 249, '<p><strong>Realistic 3d model of Old Classic Library</strong></p><p>- Native File Format: 3DS Max 2017</p><p>- OBJ and FBX format included.</p><p>- 86 Objects (without books), 3445 Books</p><p>- 18 different book models with different textures</p><p>- The model is completely ready for use visualization in 3ds max and all 3D software because this models made in both PBR workflow, Specular Glossiness and Metallic Roughness.</p><p>- High resolution over than 250 textures. and all is 8192px X 8192px for Both PBR Workflow</p><p>- PBR&gt; Specular/Glossiness includes: Diffuse, Specular, Glossiness, normal and AO map</p><p>- PBR&gt; Metallic/Roughness includes: Basecolor, Metallic, Roughness, normal and AO map</p><p>- Ready to use in all game engines and all 3D software.</p><p>- All preview renders are without any post production.</p><p>- Just open the scene and press the render button.</p><p>- No need subdivision.</p><p>- 3Ds Max files is ready to render, Render setup included.</p><p>- All Lights and vray sun are included.</p><p>- High detail model for close-up renders.</p><p>- The Model was made on the 3dsmax, and we are able to provide un collapsed modifier stack only for the 3dsmax.</p><p>- All materials and shader setup with V-Ray next .</p><p>- High quality polygonal model, correctly scaled.</p><p>- Model is fully textured with all materials applied.</p><p>- All textures and materials are included.</p><p>- Ready for use in animations</p><p>- No special plugin needed to open this scene</p><p>- Coordinates of the location of the model in space (x0, y0, z0)</p><p>- Turbosmooth modifire in stack</p><p>- No part-name confusion when importing several models into a scene.</p>', 1, 'CheckMate Pro', '3ds Max ~/!\\~ Unity 5.4 ~/!\\~ Unreal 4.10', 'OBJ ~/!\\~ FBX ~/!\\~ FBX Metallic v ~/!\\~ FBX Specular v', 1127094, 1198142, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 3, 'V-Ray', 'Polygonal Quads/Tris Geometry', '63c672852c494.jpg ~/!\\~ 63c672852c5e6.jpg ~/!\\~ 63c672852c6ec.jpg ~/!\\~ 63c672852c7d7.jpg ~/!\\~ 63c672852c8c8.jpg ~/!\\~ 63c672852c9ac.jpg', 10, 10),
(49, 'Human Head', 6, 149, '<p><strong>Human Head Muscular System soft tissue with function to produce force and motion of head and face.</strong><br>Human Head Cardiovascular System of head and neck of the human body. Blood vessels and lymphatic vessels scheme of human head and neck .<br>For people anatomy, human anatomy, biology, zoology, surgery, therapy, pathology, medicine, medical education, and health care projects design.<br><br>Human Head Full Anatomy and Skin is a high quality, photo real 3d model that will enhance detail and realism to any of your rendering projects. The model has a fully textured, detailed design that allows for close-up renders, and was originally modeled in 3ds Max 2012 and rendered with V-Ray. Renders have no postprocessing.<br>Hope you like it!<br><br>*********************************<br><i><strong>Features:</strong></i><br>- High quality polygonal model, correctly scaled for an accurate representation of the original object.<br>- Models resolutions are optimized for polygon efficiency. (In 3ds Max, the Meshsmooth function can be used to increase mesh resolution if necessary.)<br>- All colors can be easily modified.<br>- Model is fully textured with all materials applied.<br>- All textures and materials are included and mapped in every format.<br>- 3ds Max models are grouped for easy selection, and objects are logically named for ease of scene management.<br>- No part-name confusion when importing several models into a scene.<br>- No cleaning up necessaryjust drop your models into the scene and start rendering.<br>- No special plugin needed to open scene.<br>- Model does not include any backgrounds or scenes used in preview images.<br>- Units: cm<br><br>*********************************<br><i><strong>File Formats:</strong></i><br>- 3ds Max 2012 V - Ray and standard materials scenes<br>- OBJ(Multi Format)<br>- 3DS(Multi Format)<br>- Maya 2011<br>- Cinema 4D R14<br>- FBX(Multi Format)<br><br>*********************************<br><i><strong>Textures Formats:</strong></i><br>-(3.png) 8192 x 8192<br>-(46.png) 4096 x 4096<br>-(150.png) 2048 x 2048<br>-(113.png) 1024 x 1024<br><br>*********************************<br><br>3d Molier International is a team of 3D artists with over a decade of experience in the field. The company participated in various projects allowing us to learn our clients needs. Every model we build goes through thorough Quality assessment both visual and technical to make sure the assets look realistic and the models are of the best quality, which you can tell by looking at the renders none of the has any postprocessing. On the top of that all the models come with complete UVs and optimized topology, which allows you in no time alter geo or the textures if needed.<br><br>Also check out our other models, just click on our user name to see complete gallery.<br>3d_molier International 2019</p>', 1, 'Без перевірки', '3ds Max ~/!\\~ Maya ~/!\\~ Cinema 4D ~/!\\~ Unity 5.4 ~/!\\~ Unreal 4.10', '3D Studio ~/!\\~ OBJ ~/!\\~ FBX ~/!\\~ FBX Metallic v ~/!\\~ FBX Specular v', 142792, 143440, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 3, 'V-Ray', 'Polygonal Quads/Tris Geometry', '63c67977cc96e.jpg ~/!\\~ 63c67977ccad5.jpg ~/!\\~ 63c67977ccbe3.jpg ~/!\\~ 63c67977ccd62.jpg ~/!\\~ 63c67977cce6c.jpg ~/!\\~ 63c67977ccf63.jpg ~/!\\~ 63c67977cd076.jpg', 1, 11),
(50, 'Cartoon Cliff Scene', 7, 298, '<p><strong>File formats:</strong><br><br><strong>- Maya 2018 [Arnold Render]</strong><br><br><strong>-FBX</strong><br><br>**************************************************<br><br>This is a high quality model scene.<br><br>The scene is very large and has a lot of details.<br><br>Each angle detailed enough for close-up renders.<br><br>Suitable for your film,movies,Advertising and so on.<br><br>Originally modelled in Maya 2018.Final images rendered with Arnold.<br><br>**************************************************<br><br><i><strong>Features:</strong></i><br><br>- The scene contains more than 100 kinds plants and Nearly 20 kind cartoon trees.<br><br>- The animals in the scene contain two deers and birds models.<br><br>- Textures format is jpg and tga,the total number of maps is 459.<br><br>- Model is fully textured with all materials applied.<br><br>- Each object grouped and has a corresponding display layer for easy selection.<br><br>- maya Internal material and arnold material applied.<br><br>- The scene very clean,lights in the scene,just open the scene and start rendering.<br><br>**************************************************<br><br><i><strong>Poly count:</strong></i><br><br>Polygons :6307001<br><br>Vertices :7992598<br><br>**************************************************</p>', 1, 'StemCell', '3ds Max ~/!\\~ Maya ~/!\\~ Unity 5.4 ~/!\\~ Unreal 4.10', '3D Studio ~/!\\~ FBX ~/!\\~ FBX Metallic v ~/!\\~ FBX Specular v', 6307001, 7992598, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 1, 'mental ray', 'Polygonal Quads/Tris Geometry', '63c67accd82c1.jpg ~/!\\~ 63c67accd848d.jpg ~/!\\~ 63c67accd861f.jpg ~/!\\~ 63c67accdb20f.jpg ~/!\\~ 63c67accdb379.jpg ~/!\\~ 63c67accdb487.jpg ~/!\\~ 63c67accdb586.jpg', 1, 12),
(51, 'Laboratory', NULL, 359, '<p><i><strong>New Laboratory</strong></i></p><p>for the research of a new coronavirus 2019-nCoV&nbsp;</p><p>I would like to bring to your attention the most qualitatively made room / interior of the laboratory in a modern design. The style is kept as authentic as possible. In light colors, nothing distracts attention from the essence of the room.</p><p>At the same time, the filling of each element of the fittings is as unique as possible. There are practically no repetitive parts in cabinets, tables and other fittings. Due to what the interior looks realistic and very expensive.</p><p>The product consists of two laboratories, with a combined cost of $ 559, as well as many separate large parts, which are valued at over $ 700 in total. By purchasing this product, you save over $ 300!</p><p>If you like such products, good sales of them will guarantee that I will understand your interests and will make many more similar products.</p><p>I also run sales on certain days of the week and month. Keep track of prices to buy the most profitable.</p><p>This model is suitable for use in broadcast, high-res film close-up, advertising, design visualization, etc.</p><p><i><strong>Dimensions Place:</strong></i></p><p>- 1000*1100 cm;</p><p>&nbsp;&nbsp;&nbsp; SIZE MAX-file:</p><p>- 346 MB</p><p><strong>&nbsp;&nbsp;&nbsp; SCENE SIZE:</strong></p><p>- 2652671 polygons without Smooth;</p><p>- 10089401 with Smooth.</p><p><strong>&nbsp;&nbsp;&nbsp; TEXTURES:</strong></p><p>-Archive has 201 textures.</p><p>-The largest texture size up to 4096 px.</p><p>-Type textures: diffuse, metalic, reflect, roughness, bump.</p><p><strong>&nbsp;&nbsp;&nbsp; FEATURES:&nbsp;</strong></p><p>-All dimensions correspond to reality</p><p>-Lighting and cameras are included.</p><p>-Some items contain smoothing.</p>', 1, 'StemCell', '3ds Max ~/!\\~ Maya ~/!\\~ Cinema 4D ~/!\\~ Unity 5.4 ~/!\\~ test', '3D Studio ~/!\\~ FBX ~/!\\~ DXF ~/!\\~ FBX Metallic v ~/!\\~ glTF 2.0', 2652671, 2635835, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 1, 'V-Ray', 'Polygonal Quads/Tris Geometry', '63c6804329ed0.jpg ~/!\\~ 63c680432a048.jpg ~/!\\~ 63c680432a14d.jpg ~/!\\~ 63c680432a248.jpg ~/!\\~ 63c680432a32a.jpg ~/!\\~ 63c680432a422.jpg ~/!\\~ 63c680432a512.jpg ~/!\\~ 63c680432a5fc.jpg', 10, 13),
(54, 'Redshift Cyberpunk', 8, 199, '<p>Highly detailed Cinema4d Scene, compatibles with Redshift Only to boost up your projects. Includes big amount of shaders, textures and decals, and Scifi models.<br>Most of the The modeling was done in Moi3d then imported to C4d for extra Sub-d details, 90% of the texturing was procedural and done in redshift shader tree, so it can\'t be baked in FBX. I pushed the level of details as much i could to stress test Redshift so this is scene was not optimized for game engines, it\'s meant for Cinematic purposes with high-end Computers. Redshift is mandatory for this scene, the FBX comes without any textures and it was only added for people who don\'t mind texturing it by themselves in their prefered DCC tool.</p><p><strong>NFT tokenization is not allowed.</strong></p>', 1, 'StemCell', '3ds Max ~/!\\~ Maya ~/!\\~ Cinema 4D ~/!\\~ Unity 5.4', '3D Studio ~/!\\~ OBJ ~/!\\~ FBX ~/!\\~ DXF ~/!\\~ FBX Metallic v', 15556718, 16374856, 'Textures ~/!\\~ Materials ~/!\\~ UV Mapped', 1, 'V-Ray', 'Polygonal Quads only', '646c8b3eaeb5f.jpg ~/!\\~ 646c8b3eaf3a8.jpg ~/!\\~ 646c8b3eaf9c3.jpg ~/!\\~ 646c8b3eb0088.jpg ~/!\\~ 646c8b3eb02bc.jpg ~/!\\~ 646c8b3eb0598.jpg', 12, 15);

-- --------------------------------------------------------

--
-- Структура таблиці `product_statistics`
--

CREATE TABLE `product_statistics` (
  `id` int(11) NOT NULL,
  `date` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Дата виставлення продукту',
  `number_of_views` int(11) NOT NULL DEFAULT '0' COMMENT 'кількість переглядів',
  `number_of_purchases` int(11) NOT NULL DEFAULT '0' COMMENT 'кількість покупок'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `product_statistics`
--

INSERT INTO `product_statistics` (`id`, `date`, `number_of_views`, `number_of_purchases`) VALUES
(1, '08.05.2023', 15, 6),
(2, '15.01.2023', 12, 7),
(3, '08.05.2023', 5, 2),
(4, '31.12.2022', 10, 7),
(5, '25.12.2022', 1, 0),
(6, '12.04.2023', 2, 2),
(7, '25.02.2023', 2, 0),
(8, '12.04.2023', 0, 0),
(9, '22.05.2023', 13, 9),
(10, '11.03.2023', 5, 2),
(11, '22.05.2023', 8, 6),
(12, '11.02.2023', 1, 1),
(13, '25.12.2022', 4, 2),
(15, '23.05.2023', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Логін',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Пароль',
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_level` int(11) NOT NULL DEFAULT '1',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT 'Кількість зароблених грошей'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `lastname`, `firstname`, `access_level`, `photo`, `price`) VALUES
(1, 'kn211_oos@ztu.edu.ua', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'Адмін', 'Адмін', 10, '63c675ec7a377.jpg', '13116'),
(9, 'moderator@gmail.com', '6eea9b7ef19179a06954edd0f6c05ceb', 'Moderator', 'Moderator', 5, '63c6764f84844.jpg', '11150'),
(10, 'user@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'OL', 'Alex', 1, '63c677835f6e3.jpg', '4816'),
(11, 'user2@gmail.com', '1bbd886460827015e5d605ed44252251', 'User2', 'Test', 1, '63c6a88791a83.jpg', '1200'),
(12, 'ura@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'Корзун', 'Юрій', 1, NULL, '0');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `format_model`
--
ALTER TABLE `format_model`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `format_program`
--
ALTER TABLE `format_program`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `users_idfk_2` (`user_id`),
  ADD KEY `ctatistic_idfk_3` (`statistics_id`),
  ADD KEY `native_format_idfk_4` (`native_format`);

--
-- Індекси таблиці `product_statistics`
--
ALTER TABLE `product_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблиці `format_model`
--
ALTER TABLE `format_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблиці `format_program`
--
ALTER TABLE `format_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблиці `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT для таблиці `product_statistics`
--
ALTER TABLE `product_statistics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `ctatistic_idfk_3` FOREIGN KEY (`statistics_id`) REFERENCES `product_statistics` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `native_format_idfk_4` FOREIGN KEY (`native_format`) REFERENCES `format_program` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `users_idfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
